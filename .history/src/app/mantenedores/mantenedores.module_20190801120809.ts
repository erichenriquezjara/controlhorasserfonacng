import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MantenedoresRoutingModule } from './mantenedores-routing.module';
import { ColaboradoresComponent } from './colaboradores/colaboradores.component';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    MantenedoresRoutingModule,
    ColaboradoresComponent
  ]
})
export class MantenedoresModule { }
