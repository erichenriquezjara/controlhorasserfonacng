import { Injectable } from '@angular/core';
import { CanActivate, NavigationEnd } from '@angular/router';
import { Router } from '@angular/router';
import { filter } from 'rxjs/operators';

@Injectable()
export class AuthGuard implements CanActivate {

    rutas = [{ruta: '/dashboard'}, {ruta: '/form'},  {ruta: '/login'}];
    // tslint:disable-next-line:member-ordering
    static rutaActiva;
    isActivate = false;
    constructor(private router: Router) {}

    canActivate() {
          if (localStorage.getItem('isLoggedin') ) {
              return true;
          }

          this.router.navigate(['/login']);
            return false;
    }

    // tslint:disable-next-line:member-ordering
    static setVariable(variable: any) {
        AuthGuard.rutaActiva = variable;
    }
}
