import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { Router } from '@angular/router';

@Injectable()
export class AuthGuard implements CanActivate {

    rutas = [{ruta: '/dashboard'}, {ruta: '/form'},  {ruta: '/login'}];
    constructor(private router: Router) {}

    canActivate() {
        // tslint:disable-next-line:triple-equals
        const data = this.rutas.find(ob => ob['ruta'] == this.router.url);
        console.log(this.router.url);
        console.log('data: ' + data);
        if (localStorage.getItem('isLoggedin') && data.ruta.length > 0) {
            return true;
        }

        this.router.navigate(['/login']);
        return false;
    }
}
