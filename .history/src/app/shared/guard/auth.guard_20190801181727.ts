import { Injectable } from '@angular/core';
import { CanActivate, NavigationEnd } from '@angular/router';
import { Router } from '@angular/router';

@Injectable()
export class AuthGuard implements CanActivate {

    rutas = [{ruta: '/dashboard'}, {ruta: '/form'},  {ruta: '/login'}];
    constructor(private router: Router) {}

    canActivate() {
        // tslint:disable-next-line:triple-equals
        // const data = this.rutas.find(ob => ob['ruta'] == this.router.url);

        // tslint:disable-next-line:triple-equals
        const op = this.rutas.filter(data => (data.ruta == this.router.url));
        this.router.events.filter(e => e instanceof NavigationEnd).first().subscribe(() => {
            // the first navigation is complete and the URL is set
          });
        console.log(op);
        if (localStorage.getItem('isLoggedin')) {
            return true;
        }

        this.router.navigate(['/login']);
        return false;
    }
}
