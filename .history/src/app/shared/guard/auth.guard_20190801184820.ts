import { Injectable } from '@angular/core';
import { CanActivate, NavigationEnd } from '@angular/router';
import { Router } from '@angular/router';
import { filter } from 'rxjs/operators';

@Injectable()
export class AuthGuard implements CanActivate {

    rutas = [{ruta: '/dashboard'}, {ruta: '/form'},  {ruta: '/login'}];
    rutaActiva;
    constructor(private router: Router) {}

    canActivate() {
        this.router.events.pipe(filter (e => e instanceof NavigationEnd)).subscribe(() => {
            console.log(this.router.url);
            this.rutaActiva = this.router.url;
            // tslint:disable-next-line:triple-equals
            console.log(this.rutas.find(obj => obj['ruta'] == this.router.url));
            if (localStorage.getItem('isLoggedin') ) {
                return true;
            }
          });
          // tslint:disable-next-line:triple-equals


        this.router.navigate(['/login']);
            return false;

    }
}
