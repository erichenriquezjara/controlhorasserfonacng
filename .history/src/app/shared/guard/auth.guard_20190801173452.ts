import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { Router } from '@angular/router';

@Injectable()
export class AuthGuard implements CanActivate {

    rutas = [{ruta: '/dashboard'}, {ruta: '/form'}];
    constructor(private router: Router) {}

    canActivate() {
        const data = this.rutas.find(ob => ob['ruta'] === this.router.url);
        console.log('data' + data);
        if (localStorage.getItem('isLoggedin') ) {
            return true;
        }

        this.router.navigate(['/login']);
        return false;
    }
}
