import { Injectable } from '@angular/core';
import { CanActivate, NavigationEnd } from '@angular/router';
import { Router } from '@angular/router';
import { filter } from 'rxjs/operators';

@Injectable()
export class AuthGuard implements CanActivate {

    rutas = [{ruta: '/dashboard'}, {ruta: '/form'},  {ruta: '/login'}];
    // tslint:disable-next-line:member-ordering
    static rutaActiva;
    isActivate = false;
    constructor(private router: Router) {}

    canActivate() {
        this.router.events.pipe(filter (e => e instanceof NavigationEnd)).subscribe(() => {

            console.log(this.router.url);
            AuthGuard.setVariable(this.router.url);
            // tslint:disable-next-line:triple-equals
            console.log(this.rutas.find(obj => obj['ruta'] == this.router.url));


          });
          // tslint:disable-next-line:triple-equals
          console.log('ruta Activa: ' + AuthGuard.rutaActiva);
          console.log('ruta Activa: ' + AuthGuard.rutaActiva);
          console.log('ruta Activa: ' + AuthGuard.rutaActiva);
          console.log('ruta Activa: ' + AuthGuard.rutaActiva);
          console.log('ruta Activa: ' + AuthGuard.rutaActiva);
          console.log('ruta Activa: ' + AuthGuard.rutaActiva);
          console.log('ruta Activa: ' + AuthGuard.rutaActiva);
          console.log('ruta Activa: ' + AuthGuard.rutaActiva);
          console.log('ruta Activa: ' + AuthGuard.rutaActiva);
          console.log('ruta Activa: ' + AuthGuard.rutaActiva);
          console.log('ruta Activa: ' + AuthGuard.rutaActiva);
          console.log('ruta Activa: ' + AuthGuard.rutaActiva);
          console.log('ruta Activa: ' + AuthGuard.rutaActiva);
          console.log('ruta Activa: ' + AuthGuard.rutaActiva);
          console.log('ruta Activa: ' + AuthGuard.rutaActiva);
          console.log('ruta Activa: ' + AuthGuard.rutaActiva);
          console.log('ruta Activa: ' + AuthGuard.rutaActiva);
          if (localStorage.getItem('isLoggedin') ) {
              return true;
          }

          this.router.navigate(['/login']);
            return false;
    }

    // tslint:disable-next-line:member-ordering
    static setVariable(variable: any) {
        AuthGuard.rutaActiva = variable;
    }
}
