import { Injectable } from '@angular/core';
import { CanActivate, NavigationEnd } from '@angular/router';
import { Router } from '@angular/router';
import { filter } from 'rxjs/operators';

@Injectable()
export class AuthGuard implements CanActivate {

    rutas = [{ruta: '/dashboard'}, {ruta: '/form'},  {ruta: '/login'}];
    rutaActiva;
    constructor(private router: Router) {}

    canActivate() {
        this.router.events.filter(e => e instanceof NavigationEnd).first().subscribe(() => {
            // the first navigation is complete and the URL is set
          });
        if (localStorage.getItem('isLoggedin')) {
            return true;
        }

        this.router.navigate(['/login']);
        return false;
    }
}
