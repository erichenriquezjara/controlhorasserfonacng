import { Injectable } from '@angular/core';
import { CanActivate, NavigationEnd } from '@angular/router';
import { Router } from '@angular/router';

@Injectable()
export class AuthGuard implements CanActivate {

    rutas = [{ruta: '/dashboard'}, {ruta: '/form'},  {ruta: '/login'}];
    rutaActiva;
    constructor(private router: Router) {}

    canActivate() {
        // tslint:disable-next-line:triple-equals
        // const data = this.rutas.find(ob => ob['ruta'] == this.router.url);

        // tslint:disable-next-line:triple-equals
        setTimeout(() => this.rutaActiva = this.router.url);
        // tslint:disable-next-line:triple-equals
        const op = this.rutas.filter(data => (data.ruta == this.rutaActiva));

        console.log(op);
        if (localStorage.getItem('isLoggedin')) {
            return true;
        }

        this.router.navigate(['/login']);
        return false;
    }
}
