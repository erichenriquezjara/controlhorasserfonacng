import { Injectable } from '@angular/core';
import { CanActivate, NavigationEnd } from '@angular/router';
import { Router } from '@angular/router';
import { filter } from 'rxjs/operators';

@Injectable()
export class AuthGuard implements CanActivate {

    rutas = [{ruta: '/dashboard'}, {ruta: '/form'},  {ruta: '/login'}];
    public rutaActiva;
    constructor(private router: Router) {}

    canActivate() {
        const data = this.rutaActiva;
        this.router.events.pipe(filter (e => e instanceof NavigationEnd)).subscribe(() => {

            console.log(this.router.url);
            this.setVariable(this.router.url);
            // tslint:disable-next-line:triple-equals
            console.log(this.rutas.find(obj => obj['ruta'] == this.router.url));

          });
          // tslint:disable-next-line:triple-equals
          console.log('ruta Activa: ' + this.rutaActiva);
          if (localStorage.getItem('isLoggedin') ) {
            return true;
          }

        this.router.navigate(['/login']);
            return false;

    }

    setVariable(variable: any) {
        this.rutaActiva = variable;
    }
}
