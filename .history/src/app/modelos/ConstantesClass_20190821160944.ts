import { Injectable } from '@angular/core';

@Injectable()
public class ConfigurationConstants() {
  // Desarrollo.
  static _urlApi: string = 'http://localhost:8085/controlhorasserfonacapi/api/';

  // Producción:
  // static _urlApi: string = 'http://localhost:8085/controlhorasserfonacapi/api/';
};
