import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Colaborador } from './colaborador.model';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  apiUrl = 'http://localhost:5580/controlhorasserfonacapi/api/';

  constructor(private _http: HttpClient) { }

  getTrabajador(id: any = '') {
    return this._http.get<Trabajador[]>(this.apiUrl + Trabajador._url + '/' + id);
  }
}
