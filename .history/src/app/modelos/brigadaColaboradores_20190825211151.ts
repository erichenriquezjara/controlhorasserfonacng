import { Brigada } from './brigada.model';
import { Colaborador } from './colaborador.model';

export class BrigadaColaboradores {
    brigada: Brigada;
    colaboradores: Colaboradores[];
}
