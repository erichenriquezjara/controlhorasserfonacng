export class Trabajador{
    public static _url: string = 'trabajador';
    id_employee: number;
    nombres: string;
    apellido_paterno: string;
    apellido_materno: string;
    job: string;
    fecha_creacion: string;
    fecha_modificacion: string;
}