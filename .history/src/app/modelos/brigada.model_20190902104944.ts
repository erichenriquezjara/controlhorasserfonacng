import { Catalogo } from './catalogo.model';
import { Colaborador } from './colaborador.model';

export class Brigada {
    id_brigada: number;
    nombre_brigada: string;
    jefe_brigada: Colaborador;
    tipo_brigada: Catalogo;
    cancelacion: string;
}
