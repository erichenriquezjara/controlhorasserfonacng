import { Catalogo } from './catalogo.model';

export class Brigada {
    id_brigada: number;
    nombre_brigada: string;
    jefe_brigada: string;
    tipo_brigada: Catalogo;
    cancelacion: string;
}
