import { Catalogo } from './catalogo.model';

export class Colaborador {
    id_colaborador: number;
    rut: string;
    contrasena: string;
    nombres: string;
    apellido_paterno: string;
    apellido_materno: string;
    cargo: Catalogo;
    esUsuario: null;
    fecha_creacion: string;
    fecha_modificacion: string;
    cancelacion: string;
}
