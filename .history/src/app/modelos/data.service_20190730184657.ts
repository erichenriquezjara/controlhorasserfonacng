import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Colaborador } from './colaborador.model';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  apiUrl = 'http://localhost:8085/controlhorasserfonacapi/api/';

  constructor(private _http: HttpClient) { }

  getColaborador(id: any = '') {
    return this._http.get<Colaborador[]>(this.apiUrl + Colaborador._url + '/' + id);
  }
}
