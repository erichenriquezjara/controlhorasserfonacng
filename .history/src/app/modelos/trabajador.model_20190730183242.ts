export class Trabajador {
    public static _url = 'trabajador';
    id_colaborador: number;
    rut: string;
    nombres: string;
    apellido_paterno: string;
    apellido_materno: string;
    cargo: string;
    fecha_creacion: string;
    fecha_modificacion: string;
}
