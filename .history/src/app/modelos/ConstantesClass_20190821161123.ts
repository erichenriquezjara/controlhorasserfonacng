import { Injectable } from '@angular/core';

@Injectable()
class ConstantesClass() {
  // Desarrollo.
  static _urlApi: string = 'http://localhost:8085/controlhorasserfonacapi/api/';
  // Producción:
  // static _urlApi: string = 'http://serfonac.cl/controlhorasserfonacapi/api/';
};
