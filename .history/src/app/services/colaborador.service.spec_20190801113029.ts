import { TestBed } from '@angular/core/testing';
import { Colaborador } from '../modelos/colaborador.model';

describe('Colaborador.Service', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: Colaborador.Service = TestBed.get(Colaborador.Service);
    expect(service).toBeTruthy();
  });
});
