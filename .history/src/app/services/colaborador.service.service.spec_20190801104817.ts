import { TestBed } from '@angular/core/testing';

import { Colaborador.ServiceService } from './colaborador.service.service';

describe('Colaborador.ServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: Colaborador.ServiceService = TestBed.get(Colaborador.ServiceService);
    expect(service).toBeTruthy();
  });
});
