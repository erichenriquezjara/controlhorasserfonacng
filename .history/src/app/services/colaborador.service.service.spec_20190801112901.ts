import { TestBed } from '@angular/core/testing';

import { Colaborador.Service } from './colaborador.service';

describe('Colaborador.Service', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: Colaborador.Service = TestBed.get(Colaborador.ServiceService);
    expect(service).toBeTruthy();
  });
});
