
import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router, Route } from '@angular/router';
import { Observable } from 'rxjs';
import { LoginService } from './login.service';

@Injectable()
export class RoleGuard implements CanActivate {


  constructor(private loginService: LoginService, private _router: Router) {
  }

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {

    console.log('datarole: ' + next.data.role );
    if ('Admin' === next.data.role) {
      console.log('datarole: ' + next.data.role );
      return true;
    }

    // navigate to not found page
    this._router.navigate(['/login']);
    return false;
  }

}
