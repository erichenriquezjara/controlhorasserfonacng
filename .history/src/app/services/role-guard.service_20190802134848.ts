
import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router, Route } from '@angular/router';
import { Observable } from 'rxjs';
import { RolService } from './rol.service';
import { Rol } from '../modelos/rol.model';
import { Colaborador } from '../modelos/colaborador.model';

@Injectable()
export class RoleGuard implements CanActivate {

  roles: Rol[] = JSON.parse(localStorage.getItem('roles'));
  isValid: boolean = null;
  constructor(private rolService: RolService, private _router: Router) {
  }

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {


    this.roles.forEach(function(element) {
      // tslint:disable-next-line:triple-equals
      console.log('Evaluando rol: ' + element.nombre_rol);
      // tslint:disable-next-line:triple-equals
      if (element.nombre_rol == next.data.role) {
        console.log('Entro al if: ' + next.data.role);
        this.isValid = true;
      }
    });


    // navigate to not found page
    this._router.navigate(['/login']);
    this.isValid = false;

    return this.isValid;
  }

}
