
import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router, Route } from '@angular/router';
import { Observable } from 'rxjs';
import { RolService } from './rol.service';
import { Rol } from '../modelos/rol.model';
import { Colaborador } from '../modelos/colaborador.model';

@Injectable()
export class RoleGuard implements CanActivate {

  roles: Rol[];
  colaborador: Colaborador;
  constructor(private rolService: RolService, private _router: Router) {
  }

  // tslint:disable-next-line:use-lifecycle-interface
  ngOnInit() {
    this.colaborador = JSON.parse(localStorage.getItem('colaborador'));
    this.obtenerRoles(this.colaborador.id_colaborador);
    console.log(this.roles);
  }

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {

    console.log('datarole: ' + next.data.role );
    if ('Admin' === next.data.role) {
      console.log('datarole: ' + next.data.role );
      return true;
    }

    // navigate to not found page
    this._router.navigate(['/login']);
    return false;
  }

  obtenerRoles(idColaborador: number) {
    return this.rolService.getRolesbyColaborador(idColaborador).subscribe(data => this.roles = data);
  }

}
