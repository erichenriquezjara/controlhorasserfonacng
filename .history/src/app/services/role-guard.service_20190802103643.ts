
import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router, Route } from '@angular/router';
import { Observable } from 'rxjs';
import { RolService } from './rol.service';
import { Rol } from '../modelos/rol.model';
import { Colaborador } from '../modelos/colaborador.model';

@Injectable()
export class RoleGuard implements CanActivate {

  constructor(private rolService: RolService, private _router: Router) {
  }

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {

    if ('Admin' === next.data.role) {
      return true;
    }

    // navigate to not found page
    this._router.navigate(['/login']);
    return false;
  }

}
