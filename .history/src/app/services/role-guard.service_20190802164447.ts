
import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router, Route } from '@angular/router';
import { Observable } from 'rxjs';
import { RolService } from './rol.service';
import { Rol } from '../modelos/rol.model';
import { Colaborador } from '../modelos/colaborador.model';

@Injectable()
export class RoleGuard implements CanActivate {

  isValid = false;
  roles: Rol[] = JSON.parse(localStorage.getItem('roles'));
  constructor(private rolService: RolService, private _router: Router) {
  }

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    const valid = this;
    valid.isValid = false;

    this.roles.forEach(function(element) {
      // tslint:disable-next-line:triple-equals
      console.log('Evaluando rol: ' + element.nombre_rol);
      // tslint:disable-next-line:triple-equals
      if (element.nombre_rol == next.data.role && !valid.isValid) {
        console.log('Entro al if: ' + next.data.role);
        valid.isValid = true;
      }
      console.log('isValid: ' + valid.isValid);
    });


    // navigate to not found page
    // this._router.navigate(['/login']);

    return this.isValid;
  }

}
