
import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router, Route } from '@angular/router';
import { Observable } from 'rxjs';
import { RolService } from './rol.service';
import { Rol } from '../modelos/rol.model';
import { Colaborador } from '../modelos/colaborador.model';

@Injectable()
export class RoleGuard implements CanActivate {
  constructor(private rolService: RolService, private _router: Router) {
  }
  static isValid: boolean = null;

  roles: Rol[] = JSON.parse(localStorage.getItem('roles'));

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {


    this.roles.forEach(function(element) {
      // tslint:disable-next-line:triple-equals
      console.log('Evaluando rol: ' + element.nombre_rol);
      // tslint:disable-next-line:triple-equals
      if (element.nombre_rol == next.data.role) {
        console.log('Entro al if: ' + next.data.role);
        isValid = true;
      }
    });


    // navigate to not found page
    this._router.navigate(['/login']);
    isValid = false;

    return isValid;
  }

}
