import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LayoutComponent } from './layout.component';
import { AuthGuard } from '../shared';
import { RoleGuard } from '../services/role-guard.service';

const data = [{
    role: ['Administrador', 'Supervisor', 'adasdasd'],
    path: '/dashboard'
}, {
    role: ['Administrador', 'Supervisor', 'adasdasd'],
    path: '/mantenedores/colaboradores'
}, {
    role: ['Administrador', 'Supervisor', 'adasdasd'],
    path: '/mantenedores/brigadas'
}, {
    role: ['Administrador', 'Supervisor', 'adasdasd'],
    path: '/mantenedores/roles'
}, {
    role: ['Administrador', 'Supervisor', 'adasdasd'],
    path: '/mantenedores/siniestros'
}, {
    role: ['Administrador', 'Supervisor', 'adasdasd'],
    path: '/mantenedores/asistencia'
}
];

const routes: Routes = [
    {
        path: '',
        component: LayoutComponent,
        children: [
            { path: '', redirectTo: 'dashboard', pathMatch: 'prefix' },
            // tslint:disable-next-line:max-line-length
            { path: 'dashboard', loadChildren: () => import('./dashboard/dashboard.module').then(m => m.DashboardModule), canActivate: [RoleGuard],
            data},
            { path: 'charts', loadChildren: () => import('./charts/charts.module').then(m => m.ChartsModule), canActivate: [RoleGuard],
            data},
            { path: 'tables', loadChildren: () => import('./tables/tables.module').then(m => m.TablesModule) },
            { path: 'forms', loadChildren: () => import('./form/form.module').then(m => m.FormModule) },
            { path: 'bs-element', loadChildren: () => import('./bs-element/bs-element.module').then(m => m.BsElementModule) },
            { path: 'grid', loadChildren: () => import('./grid/grid.module').then(m => m.GridModule) },
            { path: 'components', loadChildren: () => import('./bs-component/bs-component.module').then(m => m.BsComponentModule) },
            { path: 'blank-page', loadChildren: () => import('./blank-page/blank-page.module').then(m => m.BlankPageModule) },
            // tslint:disable-next-line:max-line-length
            { path: 'mantenedores', loadChildren: () => import('./mantenedores/mantenedores.module').then(m => m.MantenedoresModule),
            canActivate: [RoleGuard], data }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class LayoutRoutingModule {
    roles = {'role': 'Administrador'};
}
function newFunction() {
    return '/dashboard';
}

