import { Component, OnInit } from '@angular/core';
import { Colaborador } from 'src/app/modelos/colaborador.model';
import { ToastService } from '../../../services/toast.service';
import { RespuestaAPI } from 'src/app/modelos/respuestaAPI.model';
import { RolService } from 'src/app/services/rol.service';
import { Rol } from 'src/app/modelos/rol.model';

@Component({
  selector: 'app-roles',
  templateUrl: './roles.component.html',
  styleUrls: ['./roles.component.scss']
})
export class RolesComponent implements OnInit {

  constructor ( private rolService: RolService, private toastService: ToastService ) {}

  // Flags Boleanas
  esVisiblePopUp = false;
  esNuevo = false;

  // Roles.
  rol: Rol;
  roles: Rol[];
  rolSeleccionado: Rol;

  respuesta: RespuestaAPI;

  // Variable Dummy para guardar los datos de los select.
  selectDummy: any = [
    {value: 1, label: 'VALOR 1'},
    {value: 2, label: 'VALOR 2'}
  ];

  // Columnas de la tabla de Rol.
  columnasTablaRol: any = [
    {field: 'nombre_rol', header: 'Nombre rol'},
    {field: 'fecha_creacion', header: 'Fecha de creación'}
  ];

  ngOnInit() {
    this.cargarTablaRoles();
  }

  cargarTablaRoles() {
    return this.rolService.getRoles().subscribe(
      data => {
        this.roles = data;
        console.log(this.roles);
      },
      err => console.log(err)
      );
  }

  edit(rowData) {
    this.esNuevo = false;
    this.rol = rowData;
    this.esVisiblePopUp = true;
  }

  nuevaFila() {
    this.esNuevo = true;
    this.rol = new Rol();
    this.esVisiblePopUp = true;
  }

  /*delete(rowData) {
    this.rol = rowData;
    this.rolService.eliminarColaborador(this.colaborador).subscribe(
      response => {
        this.respuesta = response[0];
        this.cargarTablaColaboradores();
        this.colaborador = new Colaborador();
        this.esVisiblePopUp = false;
        this.toastService.verificarRespuesta(this.respuesta.estado, this.respuesta.mensaje);
      },
      err => console.log(err)
    );
  }*/

  save() {
    console.log(this.rol.id_rol);

    this.rolService.registrarRol(this.rol).subscribe(
      response => {
        this.respuesta = response[0];

        if ( this.rol.id_rol === undefined) {
          this.roles.push( this.respuesta.objeto[0] );
        }

        this.rol = this.respuesta.objeto[0];
        this.esVisiblePopUp = false;
        this.toastService.verificarRespuesta(this.respuesta.estado, this.respuesta.mensaje);
      },
      err => console.log(err)
    );
  }
}

