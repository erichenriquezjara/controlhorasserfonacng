import { Component, OnInit } from '@angular/core';
import { Colaborador } from 'src/app/modelos/colaborador.model';
import { ColaboradorService } from 'src/app/services/colaborador.service';
import { ToastService } from '../../../services/toast.service';
import { RespuestaAPI } from 'src/app/modelos/respuestaAPI.model';
import { CatalogoService } from 'src/app/services/catalogo.service';
import { Catalogo } from 'src/app/modelos/catalogo.model';
import { RolService } from 'src/app/services/rol.service';
import { Rol } from 'src/app/modelos/rol.model';

@Component({
  selector: 'app-colaboradores',
  templateUrl: './colaboradores.component.html',
  styleUrls: ['./colaboradores.component.scss']
})
export class RolesComponent implements OnInit {

  constructor ( private rolService: RolService, private toastService: ToastService ) {}

  // Flags Boleanas
  esVisiblePopUp = false;
  esNuevo = false;

  // Roles.
  rol: Rol;
  roles: Rol[];
  colaboradorSeleccionado: Colaborador;
  catalogoCargos: Catalogo[];

  respuesta: RespuestaAPI;

  // Variable Dummy para guardar los datos de los select.
  selectDummy: any = [
    {value: 1, label: 'VALOR 1'},
    {value: 2, label: 'VALOR 2'}
  ];

  // Columnas de la tabla de Rol.
  columnasTablaRol: any = [
    {field: 'nombre_rol', header: 'Nombre rol'},
    {field: 'fecha_creaciom', header: 'Fecha de creación'}
  ];

  ngOnInit() {
    this.cargarTablaRoles();
    this.cargarCargos();
  }

  cargarTablaRoles() {
    return this.rolService.getRoles().subscribe(
      data => {
        this.rol = data;
        console.log(this.colaboradores);
      },
      err => console.log(err)
      );
  }

  cargarCargos() {
    return this.catalogosService.getCatalogo('CARGOS').subscribe(
      data => {
        this.catalogoCargos = data;
      },
        err => console.log(err)
      );
  }

  edit(rowData) {
    this.esNuevo = false;
    this.colaborador = rowData;
    this.esVisiblePopUp = true;
  }

  nuevaFila() {
    this.esNuevo = true;
    this.colaborador = new Colaborador();
    this.esVisiblePopUp = true;
  }

  delete(rowData) {
    this.colaborador = rowData;
    this.colaboradorService.eliminarColaborador(this.colaborador).subscribe(
      response => {
        this.respuesta = response[0];
        this.cargarTablaColaboradores();
        this.colaborador = new Colaborador();
        this.esVisiblePopUp = false;
        this.toastService.verificarRespuesta(this.respuesta.estado, this.respuesta.mensaje);
      },
      err => console.log(err)
    );
  }

  save() {
    console.log(this.colaborador.id_colaborador);

    this.colaboradorService.registrarColaborador(this.colaborador).subscribe(
      response => {
        this.respuesta = response[0];

        if ( this.colaborador.id_colaborador === undefined) {
          this.colaboradores.push( this.respuesta.objeto[0] );
        }

        this.colaborador = this.respuesta.objeto[0];
        this.esVisiblePopUp = false;
        this.toastService.verificarRespuesta(this.respuesta.estado, this.respuesta.mensaje);
      },
      err => console.log(err)
    );
  }
}

