import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ColaboradoresComponent } from './colaboradores/colaboradores.component';
import { BrigadasComponent } from './brigadas/brigadas.component';
import { RolesComponent } from './roles/roles.component';
import { RoleGuard } from 'src/app/services/role-guard.service';
import { SiniestrosComponent } from './siniestros/siniestros.component';
import { AsistenciaComponent } from './asistencia/asistencia.component';

const routes: Routes = [
  { path: 'colaboradores', component: ColaboradoresComponent, canActivate: [RoleGuard]  },
  { path: 'brigadas', component: BrigadasComponent, canActivate: [RoleGuard]  },
  { path: 'roles', component: RolesComponent, canActivate: [RoleGuard]  },
  { path: 'siniestros', component: SiniestrosComponent, canActivate: [RoleGuard]  },
  { path: 'asistencia', component: AsistenciaComponent, canActivate: [RoleGuard]  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MantenedoresRoutingModule { }
