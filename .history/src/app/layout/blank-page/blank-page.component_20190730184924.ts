import { Component, OnInit } from '@angular/core';
import { Colaborador } from 'src/app/modelos/colaborador.model';
import { DataService } from 'src/app/modelos/data.service';

@Component({
    selector: 'app-blank-page',
    templateUrl: './blank-page.component.html',
    styleUrls: ['./blank-page.component.scss']
})
export class BlankPageComponent implements OnInit {

    colaboradores: Colaborador[];

    constructor(private dataService: DataService) {}

    ngOnInit() {
        console.log(this.dataService.getColaborador(1));
        return this.dataService.getColaborador(1).subscribe(data => this.colaboradores = data);
        // return this.dataService.getColaborador(1);
    }
}
