import { Component, OnInit } from '@angular/core';
import { Colaborador } from 'src/app/modelos/colaborador.model';
import { DataService } from 'src/app/modelos/data.service';
import { LoginService } from 'src/app/services/login.service';

@Component({
    selector: 'app-blank-page',
    templateUrl: './blank-page.component.html',
    styleUrls: ['./blank-page.component.scss']
})
export class BlankPageComponent implements OnInit {

    colaboradores: Colaborador[];
    login = [{rut: '16329970-4', contrasena: '123456'}];

    constructor(private dataService: DataService, private loginService: LoginService) {}

    ngOnInit() {

        // console.log(this.dataService.getColaborador(1).subscribe(data => this.colaboradores = data));
        // return this.dataService.getColaborador(1).subscribe(data => this.colaboradores = data);
        return this.loginService.getLogin(this.login).subscribe(data => this.colaboradores = data);
        // return this.dataService.getColaborador(1);
    }
}
