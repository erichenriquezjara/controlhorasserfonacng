import { Component, OnInit } from '@angular/core';
import { Trabajador } from 'src/app/modelos/colaborador.model';
import { DataService } from 'src/app/modelos/data.service';

@Component({
    selector: 'app-blank-page',
    templateUrl: './blank-page.component.html',
    styleUrls: ['./blank-page.component.scss']
})
export class BlankPageComponent implements OnInit {

    trabajadores: Trabajador[];

    constructor(private dataService: DataService) {}

    ngOnInit() {
        return this.dataService.getTrabajador(1).subscribe(data => this.trabajadores = data);
    }
}
