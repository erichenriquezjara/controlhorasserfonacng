import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { NgbDropdownModule } from '@ng-bootstrap/ng-bootstrap';
import { LayoutRoutingModule } from './layout-routing.module';
import { LayoutComponent } from './layout.component';
import { DialogModule } from 'primeng/dialog';
import { ButtonModule } from 'primeng/button';
import { TableModule } from 'primeng/table';
import { CheckboxModule } from 'primeng/checkbox';
import { CalendarModule } from 'primeng/calendar';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { HeaderComponent } from './components/header/header.component';
import { ElegirColaboradoresComponent } from './components/elegir-colaboradores/elegir-colaboradores.component';
import { SeleccionarFechaComponent } from './components/seleccionar-fecha/seleccionar-fecha.component';
import { FormsModule } from '@angular/forms';
import { NuevoColaboradorComponent } from './components/nuevo-colaborador/nuevo-colaborador.component';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { UpdateDateInterceptor } from '../services/update-date.interceptor';
import { InputMaskModule } from 'primeng/inputmask';

@NgModule({
    declarations: [
        LayoutComponent,
        SidebarComponent,
        HeaderComponent,
        ElegirColaboradoresComponent,
        SeleccionarFechaComponent,
        NuevoColaboradorComponent
    ],
    imports: [
        FormsModule,
        CommonModule,
        LayoutRoutingModule,
        TranslateModule,
        NgbDropdownModule,
        DialogModule,
        ButtonModule,
        TableModule,
        CheckboxModule,
        CalendarModule,
        InputMaskModule
    ],
    exports: [
        ElegirColaboradoresComponent,
        SeleccionarFechaComponent
    ]

})
export class LayoutModule {}
