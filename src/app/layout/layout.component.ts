import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
    selector: 'app-layout',
    templateUrl: './layout.component.html',
    styleUrls: ['./layout.component.scss']
})
export class LayoutComponent implements OnInit {

    mostrar = false;
    collapedSideBar: boolean;

    constructor( private router: Router) {
        if ( !localStorage.getItem('colaborador') ) {
            this.router.navigate(['/login']);
        } else {
            this.mostrar = true;
        }
    }

    ngOnInit() {}

    receiveCollapsed($event) {
        this.collapedSideBar = $event;
    }
}
