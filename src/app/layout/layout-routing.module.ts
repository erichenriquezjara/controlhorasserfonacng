import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LayoutComponent } from './layout.component';
import { AuthGuard } from '../shared';
import { Role } from '../modelos/role';

const routes: Routes = [
    {
        path: '',
        component: LayoutComponent,
        children: [
            {
                path: 'dashboard',
                loadChildren: () => import('./dashboard/dashboard.module').then(m => m.DashboardModule),
                canActivate : [AuthGuard],
                data: { roles: [Role.Supervisor , Role.Admin, Role.JefeBrigada ] }

            },
            {
                path: 'mantenedores',
                loadChildren: () => import('./mantenedores/mantenedores.module').then(m => m.MantenedoresModule),
                canActivate : [AuthGuard],
                data: { roles: [Role.Admin, Role.Supervisor, Role.JefeBrigada]}
            }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class LayoutRoutingModule {
}
function newFunction() {
    return '/dashboard';
}

