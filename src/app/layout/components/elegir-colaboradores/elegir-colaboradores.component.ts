import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { ColaboradorService } from 'src/app/services/colaborador.service';
import { Subscription } from 'rxjs';
import { Brigada } from 'src/app/modelos/brigada.model';
import { Colaborador } from 'src/app/modelos/colaborador.model';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/api';
import { SiniestrosService } from 'src/app/services/siniestros.service';
import { Siniestro } from 'src/app/modelos/siniestro.model';


@Component({
  selector: 'app-elegir-colaboradores',
  templateUrl: './elegir-colaboradores.component.html',
  styleUrls: ['./elegir-colaboradores.component.scss']
})

export class ElegirColaboradoresComponent implements OnInit {

  @Input()
  brigadaSeleccionada: Brigada;
  siniestroSeleccionado: Siniestro;

  @Output() seleccionTerminada = new EventEmitter();

  // Lista de colaboradores por Brigada.
  colaboradoresPorBrigada: Colaborador[];

  // Lista de colaboradores seleccionados.
  colaboradoresSeleccionados: Colaborador[];

  colaborador: Colaborador;

  // Esta variable es para invocar el método que cambia el cursor y anima.
  busy: Subscription;

  // Columnas de la tabla de Colaboradores.
  columnasTablaColaborador: any = [
    // {field: 'check', header: 'check'},
    {field: 'rut', header: 'RUT'},
    {field: 'nombres', header: 'Nombres'},
    {field: 'apellido_paterno', header: 'Apellido P.'},
    {field: 'apellido_materno', header: 'Apellido M.'},
    {field: 'cargo', header: 'Cargo'},
    // {field: 'es_usuario', header: 'Es usuario'},
  ];

  constructor(
    private colaboradorService: ColaboradorService,
    private siniestroService: SiniestrosService,
    private dynamicDialogConfig: DynamicDialogConfig,
    private dynamicDialogRef: DynamicDialogRef) { }

  ngOnInit() {
    if ( this.dynamicDialogConfig.data.Brigada ) {
      this.brigadaSeleccionada = this.dynamicDialogConfig.data.Brigada;
      this.siniestroSeleccionado = this.dynamicDialogConfig.data.Siniestro;
    }

    this.iniciarComponente();
    //   this.colaborador = {
    //     apellido_materno: 'Peréz',
    //     apellido_paterno: 'Rivarola',
    //     contrasena: 'e10adc3949ba59abbe56e057f20f883e',
    //     es_usuario: 1,
    //     id_colaborador: 32,
    //     nombres: 'Alfonso',
    //     rut: '123123-5',

    //  };
    //  this.colaboradoresSeleccionados = new Array<Colaborador>();
    //  this.colaboradoresSeleccionados.push(this.colaborador);

  }

  iniciarComponente() {
    this.cargarColaboradoresPorBrigada();
    this.cargarColaboradoresSeleccionados(this.siniestroSeleccionado);
  }

  cargarColaboradoresPorBrigada() {
    this.busy = this.colaboradorService.getColaboradoresPorBrigada(this.brigadaSeleccionada).subscribe(
      data => {
        this.colaboradoresPorBrigada = data;
        console.log(this.colaboradoresPorBrigada);
      },
      err => console.log(err)
      );
    return;
  }

  cargarColaboradoresSeleccionados(siniestro: Siniestro) {
    this.busy = this.colaboradorService.colaboradoresSiniestro(siniestro.id_siniestro).subscribe(
      data => {
        console.log(data);
          // this.colaboradoresSeleccionados = new Array<Colaborador>();
          this.colaboradoresSeleccionados = data;
        //  data.forEach(element => {
        //    this.colaboradoresSeleccionados.push(element);
        //  });
      },
      err => console.log(err)
    );
  }

  enviarSeleccion() {
    // Llamado por tag
    this.seleccionTerminada.emit(this.colaboradoresSeleccionados);

    // Llamando por DialogService
    this.dynamicDialogRef.close(this.colaboradoresSeleccionados);
  }
}
