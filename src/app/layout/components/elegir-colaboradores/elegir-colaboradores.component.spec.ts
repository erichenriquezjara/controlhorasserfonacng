import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ElegirColaboradoresComponent } from './elegir-colaboradores.component';

describe('ElegirColaboradoresComponent', () => {
  let component: ElegirColaboradoresComponent;
  let fixture: ComponentFixture<ElegirColaboradoresComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ElegirColaboradoresComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ElegirColaboradoresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
