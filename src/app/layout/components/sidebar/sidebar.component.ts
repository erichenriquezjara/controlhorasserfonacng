import { Component, Output, EventEmitter, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';

@Component({
    selector: 'app-sidebar',
    templateUrl: './sidebar.component.html',
    styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {
    isActive: boolean;
    collapsed: boolean;
    showMenu: string;
    pushRightClass: string;

    // menus = [
    //     {
    //         'nombre': 'Dashboard',
    //         'link': '/dashboard',
    //         'iClass': 'fa fa-fw fa-dashboard'
    //     },
    //     {
    //         'nombre': 'Asistencia',
    //         'link': '/mantenedores/asistencia',
    //         'iClass': 'fa fa-users'
    //     },
    //     {
    //         'nombre': 'Registro de Actividades',
    //         'link': '/mantenedores/siniestros',
    //         'iClass': 'fa fa-plus',
    //     },
    //     {
    //         'nombre': 'Mantenedores',
    //         'link': '/mantenedores/asistencia',
    //         'iClass': 'fa fa-plus',
    //         'submenus': [
    //             {
    //                 'nombre': 'Supervisores',
    //                 'link': '/mantenedores/supervisores',
    //                 'iClass': 'fa fa-plus'
    //             },
    //             {
    //                 'nombre': 'Colaboradores',
    //                 'link': '/mantenedores/colaboradores',
    //                 'iClass': 'fa fa-plus'
    //             },
    //             {
    //                 'nombre': 'Brigadas',
    //                 'link': '/mantenedores/brigadas',
    //                 'iClass': 'fa fa-plus'
    //             },
    //             {
    //                 'nombre': 'Roles',
    //                 'link': '/mantenedores/roles',
    //                 'iClass': 'fa fa-plus'
    //             }
    //         ]
    //     }
    // ];

    menus = JSON.parse(localStorage.getItem('colaborador'));


    @Output() collapsedEvent = new EventEmitter<boolean>();

    constructor(private translate: TranslateService, public router: Router) {
        this.router.events.subscribe(val => {
            if (
                val instanceof NavigationEnd &&
                window.innerWidth <= 992 &&
                this.isToggled()
            ) {
                this.toggleSidebar();
            }
        });
    }

    ngOnInit() {
        this.isActive = false;
        this.collapsed = false;
        this.showMenu = '';
        this.pushRightClass = 'push-right';
        this.menus = this.menus.menus;
    }


    eventCalled() {
        this.isActive = !this.isActive;
    }

    addExpandClass(element: any) {
        if (element === this.showMenu) {
            this.showMenu = '0';
        } else {
            this.showMenu = element;
        }
    }

    toggleCollapsed() {
        this.collapsed = !this.collapsed;
        this.collapsedEvent.emit(this.collapsed);
    }

    isToggled(): boolean {
        const dom: Element = document.querySelector('body');
        return dom.classList.contains(this.pushRightClass);
    }

    toggleSidebar() {
        const dom: any = document.querySelector('body');
        dom.classList.toggle(this.pushRightClass);
    }

    rltAndLtr() {
        const dom: any = document.querySelector('body');
        dom.classList.toggle('rtl');
    }

    changeLang(language: string) {
        this.translate.use(language);
    }

    onLoggedout() {
        localStorage.removeItem('isLoggedin');
    }
}
