import { Component, OnInit } from '@angular/core';
import { Siniestro } from 'src/app/modelos/siniestro.model';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { SiniestrosService } from 'src/app/services/siniestros.service';
import { ToastService } from 'src/app/services/toast.service';
import { RespuestaAPI } from 'src/app/modelos/respuestaAPI.model';
import { DynamicDialogRef, DynamicDialogConfig, SelectItem } from 'primeng/api';
import { Brigada } from 'src/app/modelos/brigada.model';
import { BrigadaService } from 'src/app/services/brigada.service';
import { Colaborador } from 'src/app/modelos/colaborador.model';
import { Catalogo } from 'src/app/modelos/catalogo.model';
import { CatalogoService } from 'src/app/services/catalogo.service';

@Component({
  selector: 'app-nueva-actividad',
  templateUrl: './nueva-actividad.component.html',
  styleUrls: ['./nueva-actividad.component.scss']
})
export class NuevaActividadComponent implements OnInit {

  colaboradorJefe: Colaborador;
  siniestro: Siniestro;
  actividadForm: FormGroup;
  respuesta: RespuestaAPI;
  siniestros: Siniestro[];
  // brigadasSelect: SelectItem[];
  brigadasSelect: number;
  brigada: Brigada;

  catalogoLugaresItem: SelectItem[];
  catalogoLugares: Catalogo[];
  catalogoActividades: Catalogo[];
  lugarSeleccionado: Catalogo;
  actividadSeleccionada: Catalogo;

  constructor(private _builder: FormBuilder,
    private siniestroService: SiniestrosService,
    private brigadaService: BrigadaService,
    private toastService: ToastService,
    private catalogoService: CatalogoService,
    public ref: DynamicDialogRef,
    public config: DynamicDialogConfig) {

    this.actividadForm = this._builder.group({
      codigo_actividad: [''],
      centro_costo: [''],
      fuerza_combate: [''],
      id_brigada: [''],
      nro_foco: ['', Validators.required],
      lugar_destino: ['']
    });
  }

  ngOnInit() {
    console.log('Init');
    console.log(this.config.data);
    if ( this.config.data.brigadaSeleccionada ) {
      this.brigada = this.config.data.brigadaSeleccionada;
    }

    this.actividadForm.patchValue({
      fuerza_combate: this.brigada.tipo_brigada.valor1,
      id_brigada: this.brigada.id_brigada
    });

    this.cargarCatalogoLugares();
    this.cargarCatalogoActividades();
  }

  cargarCatalogoLugares() {
    if ( localStorage.getItem('CAT_LUGAR_DESTINO') == null ) {
      this.catalogoService.getCatalogo('LUGAR_DESTINO').subscribe(
        data => {
          this.catalogoLugares = data;
          this.catalogoLugares.forEach(element => {
            element.labelExtra = element.valor1 + ' - ' + element.valor;
          });          
          localStorage.setItem('CAT_LUGAR_DESTINO', JSON.stringify(data) );
          this.lugarSeleccionado = this.catalogoLugares[0];
        },
          err => console.log(err)
        );
    } else {
      this.catalogoLugares = JSON.parse( localStorage.getItem('CAT_LUGAR_DESTINO') );
      this.lugarSeleccionado = this.catalogoLugares[0];
    }
    return ;
  }

  cargarCatalogoActividades() {
    if ( localStorage.getItem('CAT_ACTIVIDADES') == null ) {
      this.catalogoService.getCatalogo('ACTIVIDADES').subscribe(
        data => {
          this.catalogoActividades = data;
          this.catalogoActividades.forEach(element => {
            element.labelExtra = element.valor1 + ' - ' + element.valor;
          });
          localStorage.setItem('CAT_ACTIVIDADES', JSON.stringify(this.catalogoActividades) );
          this.actividadSeleccionada = this.catalogoActividades[0];
        },
          err => console.log(err)
        );
    } else {
      this.catalogoActividades = JSON.parse( localStorage.getItem('CAT_ACTIVIDADES') );
      this.actividadSeleccionada = this.catalogoActividades[0];
    }
    return ;
  }

  save() {
    this.siniestro = new Siniestro();
    console.log(this.actividadForm.value.lugar_destino);

    if ( this.actividadForm.value.lugar_destino.valor1 === '-') {
      this.toastService.advertencia('Debe seleccionar un Lugar o Destino');
      return;
    } else {
      this.actividadForm.value.lugar_destino = this.actividadForm.value.lugar_destino.id_catalogo;
    }

    if ( this.actividadForm.value.codigo_actividad.valor1 === '-' ) {
      this.toastService.advertencia('Debe seleccionar una Actividad');
      return;
    } else {
      this.actividadForm.value.codigo_actividad = this.actividadForm.value.codigo_actividad.valor1;
    }

    this.siniestroService.registrarSiniestro(this.actividadForm.value).subscribe(
      response => {
        this.respuesta = response[0];
        this.siniestro = this.respuesta.objeto[0];
        this.toastService.verificarRespuesta(this.respuesta.estado, this.respuesta.mensaje);
        this.ref.close(this.siniestro);
      },
      err => console.log(err)
    );

  }

  cargarFuerzaCombateBrigada( brigada ) {

    return 1;
  }

}
