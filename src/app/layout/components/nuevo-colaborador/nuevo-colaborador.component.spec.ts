import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { NuevoColaboradorComponent } from './nuevo-colaborador.component';

describe('NuevoColaboradorComponent', () => {
  let component: NuevoColaboradorComponent;
  let fixture: ComponentFixture<NuevoColaboradorComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ NuevoColaboradorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NuevoColaboradorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
