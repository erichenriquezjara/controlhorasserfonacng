import { Component, OnInit, Input } from '@angular/core';
import { environment } from 'src/environments/environment';
import { DynamicDialogRef, DynamicDialogConfig } from 'primeng/api';
import { ToastService } from 'src/app/services/toast.service';
import { Rol } from 'src/app/modelos/rol.model';

@Component({
  selector: 'app-seleccionar-fecha',
  templateUrl: './seleccionar-fecha.component.html',
  styleUrls: ['./seleccionar-fecha.component.scss']
})
export class SeleccionarFechaComponent implements OnInit {

  @Input()
  fechaCalendario: Date = null;

  fechaCalendarioString = '';

  // Variable que guarda el ROL del usuario Logeado
  rol: Rol;

  constructor(
    private dynamicDialogRef: DynamicDialogRef,
    private dynamicDialogConfig: DynamicDialogConfig,
    private toastService: ToastService
  ) { }

  es: any = environment.locale_es;

  ngOnInit() {
    console.log('this.fechaCalendario');
    console.log(this.dynamicDialogConfig.data.fechaActual);
    this.rol = JSON.parse( localStorage.getItem('colaborador') ).role[0];
    if ( this.dynamicDialogConfig.data.fechaActual ) {
      this.fechaCalendario = this.dynamicDialogConfig.data.fechaActual;
    }
  }

  enviarSeleccion() {
    // Si es administrador siempre puede editar.
    if ( this.rol.id_rol == 1 ) {
      const fechaAux = this.fechaCalendarioString.split('/');
      const fechaFinal = fechaAux[1] + '/' + fechaAux[0] + '/' + fechaAux[2];
      this.fechaCalendario = new Date(fechaFinal);
    }

    if ( this.validarFecha(this.fechaCalendario) ) {
      this.dynamicDialogRef.close(this.fechaCalendario);
    } else {
      this.toastService.advertencia('La fecha no es válida');
    }
  }

  validarFecha ( d: Date ): boolean {
    if (Object.prototype.toString.call(d) === '[object Date]') {
      // it is a date
      if (isNaN(d.getTime())) {  // d.valueOf() could also work
        return false;
      } else {
        return true;
      }
    } else {
      return false;
    }
  }

}
