import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../router.animations';
import { environment } from 'src/environments/environment';
import { HorasExtrasService } from 'src/app/services/horas-extras.service';
import * as moment from 'moment';

@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.scss'],
    animations: [routerTransition()],
    providers: [HorasExtrasService]
})
export class DashboardComponent implements OnInit {
    public alerts: Array<any> = [];
    public sliders: Array<any> = [];

    es: any;
    idUsuarioLogeado = 0;
    fDesde: Date;
    fHasta: Date;
    fDesdeString: string;
    fHastaString: string;

    columnasHorasExtrasPorZona = [
        { field: 'zona', header: 'ZONA' },
        { field: 'total_sr', header: 'TOTAL SIN RETORNO' },
        { field: 'total_cr', header: 'TOTAL CON RETORNO' }
    ];

    resultadoHorasExtrasPorZona = [];

    constructor(
        private horaExtrasService: HorasExtrasService
    ) {
        /*
        this.sliders.push(
            {
                imagePath: 'assets/images/slider1.jpg',
                label: 'First slide label',
                text:
                    'Nulla vitae elit libero, a pharetra augue mollis interdum.'
            },
            {
                imagePath: 'assets/images/slider2.jpg',
                label: 'Second slide label',
                text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.'
            },
            {
                imagePath: 'assets/images/slider3.jpg',
                label: 'Third slide label',
                text:
                    'Praesent commodo cursus magna, vel scelerisque nisl consectetur.'
            }
        );

        this.alerts.push(
            {
                id: 1,
                type: 'success',
                message: `Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                Voluptates est animi quibusdam praesentium quam, et perspiciatis,
                consectetur velit culpa molestias dignissimos
                voluptatum veritatis quod aliquam! Rerum placeat necessitatibus, vitae dolorum`
            },
            {
                id: 2,
                type: 'warning',
                message: `Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                Voluptates est animi quibusdam praesentium quam, et perspiciatis,
                consectetur velit culpa molestias dignissimos
                voluptatum veritatis quod aliquam! Rerum placeat necessitatibus, vitae dolorum`
            }
        );
        */
    }

    ngOnInit() {
        this.idUsuarioLogeado = JSON.parse( localStorage.getItem('colaborador') ).id_colaborador;
        this.es = environment.locale_es;
        this.actualizarDashboard(this.idUsuarioLogeado, this.fDesde, this.fDesde);
    }

    actualizarDashboard(idUsuarioLogeado: any, fDesde: any, fHasta: any){
        this.fDesdeString = moment(fDesde).format('YYYY-MM-DD');
        this.fHastaString = moment(fHasta).format('YYYY-MM-DD');
        if(fDesde == null || fHasta == null){
            this.fDesdeString = moment().subtract(30,'d').format('YYYY-MM-DD');
            this.fHastaString = moment().format('YYYY-MM-DD');
        }
        this.horaExtrasService.horasExtrasPorZona(idUsuarioLogeado, this.fDesdeString ,this.fHastaString).subscribe(
            ok => {
                this.resultadoHorasExtrasPorZona = ok;
                console.log(ok);
            },
            error =>{

            }
        );
    }

    /*
    public closeAlert(alert: any) {
        const index: number = this.alerts.indexOf(alert);
        this.alerts.splice(index, 1);
    }
    */
}
