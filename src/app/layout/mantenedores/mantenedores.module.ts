import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MantenedoresRoutingModule } from './mantenedores-routing.module';
import { ColaboradoresComponent } from './colaboradores/colaboradores.component';
import { InputTextModule } from 'primeng/inputtext';
import { PanelModule } from 'primeng/panel';
import { TableModule } from 'primeng/table';
import { ToastModule } from 'primeng/toast';
import { CheckboxModule } from 'primeng/checkbox';
import { PickListModule } from 'primeng/picklist';
import { DropdownModule } from 'primeng/dropdown';
import { InputMaskModule } from 'primeng/inputmask';
import { ButtonModule } from 'primeng/button';
import { DialogModule } from 'primeng/dialog';
import { BrigadasComponent } from './brigadas/brigadas.component';
import { RolesComponent } from './roles/roles.component';
import { NgBusyModule } from 'ng-busy';
import { SiniestrosComponent } from './siniestros/siniestros.component';
import { AsistenciaComponent } from './asistencia/asistencia.component';
import { CalendarModule } from 'primeng/calendar';
import { LayoutModule } from '../layout.module';
import { ElegirColaboradoresComponent } from '../components/elegir-colaboradores/elegir-colaboradores.component';
import { NuevaActividadComponent } from '../components/nueva-actividad/nueva-actividad.component';
import { FormModule } from '../form/form.module';
import { SupervisorComponent } from './supervisor/supervisor.component';
import { SeleccionarFechaComponent } from '../components/seleccionar-fecha/seleccionar-fecha.component';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { NgxLoadingModule, ngxLoadingAnimationTypes } from 'ngx-loading';
import { HorasExtrasComponent } from './horas-extras/horas-extras.component';
import { HorasextrasPipe } from 'src/app/src/Pipes/horasextras.pipe';
import { HorasExtrasBrigadaComponent } from './horas-extras-brigada/horas-extras-brigada.component';

@NgModule({
  declarations: [
    ColaboradoresComponent,
    BrigadasComponent,
    RolesComponent,
    SiniestrosComponent,
    AsistenciaComponent,
    NuevaActividadComponent,
    SupervisorComponent,
    HorasExtrasComponent,
    HorasextrasPipe,
    HorasExtrasBrigadaComponent
  ],
  imports: [
    CommonModule,
    MantenedoresRoutingModule,
    InputTextModule,
    TableModule,
    CalendarModule,
    ToastModule,
    PanelModule,
    DropdownModule,
    PickListModule,
    CheckboxModule,
    ButtonModule,
    DialogModule,
    FormsModule,
    CalendarModule,
    ConfirmDialogModule,
    InputMaskModule,
    NgBusyModule.forRoot({
      message: 'Por Favor, espere.',
      delay: 50,
      minDuration: 600,
      templateNgStyle: { 'background-color': 'black', 'position': 'absolute' }
    }),
    NgxLoadingModule.forRoot({
      animationType: ngxLoadingAnimationTypes.circleSwish
    }),
    LayoutModule,
    ReactiveFormsModule,
    FormModule
  ],
  entryComponents: [
    ElegirColaboradoresComponent,
    NuevaActividadComponent,
    SeleccionarFechaComponent
  ]

})
export class MantenedoresModule { }
