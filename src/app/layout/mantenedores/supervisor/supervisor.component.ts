import { Component, OnInit } from '@angular/core';
import { Colaborador } from 'src/app/modelos/colaborador.model';
import { ColaboradorService } from 'src/app/services/colaborador.service';
import { ToastService } from '../../../services/toast.service';
import { RespuestaAPI } from 'src/app/modelos/respuestaAPI.model';
import { Subscription } from 'rxjs';
import { BrigadaService } from 'src/app/services/brigada.service';
import { Brigada } from 'src/app/modelos/brigada.model';

@Component({
  selector: 'app-supervisor',
  templateUrl: './supervisor.component.html',
  styleUrls: ['./supervisor.component.scss']
})
export class SupervisorComponent implements OnInit {

  constructor(
    private colaboradorService: ColaboradorService,
    private brigadaService: BrigadaService,
    private toastService: ToastService) { }

  // Para los loadings
  loadingTabla = false;
  loadingBrigadas = false;

  // Colaboradores (Supervisores)
  colaboradores: Colaborador[];
  colaboradorSeleccionado: Colaborador;

  // Brigadas
  brigadaActuales: Brigada[];
  brigadaSinAsignar: Brigada[];

  // RespuestaAPI
  respuesta: RespuestaAPI;

  // Columnas de la tabla de Colaborador.
  columnasTablaColaborador: any = [
    { field: 'rut', header: 'RUT' },
    { field: 'nombres', header: 'Nombres' },
    { field: 'apellido_paterno', header: 'Apellido P.' },
    { field: 'apellido_materno', header: 'Apellido M.' }
  ];

  ngOnInit() {
    this.cargarTablaColaboradores();
  }

  cargarTablaColaboradores() {
    this.loadingTabla = true;
    this.colaboradorService.getColaboradoresPorRol('supervisor').subscribe(
      data => {
        this.colaboradores = data;
        console.log(this.colaboradores);
      },
      err => console.log(err)
    ).add(() => {
      this.loadingTabla = false;
    });
    return;
  }

  onRowSelect() {
    console.log(this.colaboradorSeleccionado);
    this.brigadasActuales();
  }

  brigadasActuales() {
    this.loadingBrigadas = true;
    this.brigadaService.getBrigadasPorSupervisor(this.colaboradorSeleccionado.id_colaborador).subscribe(
      response => {
        this.brigadaActuales = response;
      }
    ).add(() => {
      this.loadingBrigadas = false;
    });

    this.loadingBrigadas = true;
    this.brigadaService.getBrigadasSinSupervisor().subscribe(
      response => {
        this.brigadaSinAsignar = response;
      }
    ).add(() => {
      this.loadingBrigadas = false;
    });
  }

  guardarBrigadasPorSupervisor() {
    this.loadingBrigadas = true;
    this.brigadaService.registrarBrigadasPorSupervisor(this.brigadaActuales, this.colaboradorSeleccionado).subscribe(
      response => {
        this.respuesta = response[0];
        this.toastService.verificarRespuesta(this.respuesta.estado, this.respuesta.mensaje);
      },
      err => console.log(err)
    ).add(() => {
      this.loadingBrigadas = false;
    });
  }
}
