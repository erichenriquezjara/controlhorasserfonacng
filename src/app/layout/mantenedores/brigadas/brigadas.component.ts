import { Component, OnInit } from '@angular/core';
import { Brigada } from 'src/app/modelos/brigada.model';
import { Colaborador } from 'src/app/modelos/colaborador.model';
import { Catalogo } from 'src/app/modelos/catalogo.model';
import { BrigadaService } from 'src/app/services/brigada.service';
import { ColaboradorService } from 'src/app/services/colaborador.service';
import { ToastService } from '../../../services/toast.service';
import { RespuestaAPI } from 'src/app/modelos/respuestaAPI.model';
import { PickListModule } from 'primeng/picklist';
import { CatalogoService } from 'src/app/services/catalogo.service';
import { Subscription } from 'rxjs';
import { ConfirmationService } from 'primeng/api';

@Component({
  selector: 'app-brigadas',
  templateUrl: './brigadas.component.html',
  styleUrls: ['./brigadas.component.scss']
})
export class BrigadasComponent implements OnInit {

  constructor(
    private colaboradorService: ColaboradorService,
    private catalogosService: CatalogoService,
    private brigadaService: BrigadaService,
    private toastService: ToastService,
    private confirmationService: ConfirmationService) { }

  // Flags Boleanas
  esVisiblePopUp = false;
  esNuevo = false;

  // Para los loadings
  loadingBrigadas = false;
  loadingIntegrantes = false;

  // Brigadas.
  brigada: Brigada;
  brigadas: Brigada[];
  brigadaSeleccionado: Brigada;

  respuesta: RespuestaAPI;
  catalogoTipoBrigada: Catalogo[];

  fuenteDeColaboradores: Colaborador[];
  destinoColaboradores: Colaborador[];
  jefesBrigada: Colaborador[];

  // Columnas de la tabla de Brigada.
  columnasTablaBrigada: any = [
    { field: 'nombre_brigada', header: 'Nombre Brigada' },
    { field: 'jefe_brigada', header: 'Jefe Brigada' },
    { field: 'tipo_brigada', header: 'Tipo Brigada' }
  ];

  cargarColaboradoresFuente() {
    this.loadingIntegrantes = true;
    this.colaboradorService.getColaboradoresSinBrigada().subscribe(
      data => {
        this.fuenteDeColaboradores = data;
      },
      err => console.log(err)
    ).add(() => {
      this.loadingIntegrantes = false;
    });
    return;
  }

  cargarTipoBrigada() {
    this.catalogosService.getCatalogo('TIPO BRIGADA').subscribe(
      data => {
        this.catalogoTipoBrigada = data;
      },
      err => console.log(err)
    );
    return;
  }

  cargarColaboradoresDestino() {
    this.loadingIntegrantes = true;
    this.colaboradorService.getColaboradoresPorBrigada(this.brigadaSeleccionado).subscribe(
      data => {
        this.destinoColaboradores = data;
      },
      err => console.log(err)
    ).add(() => {
      this.loadingIntegrantes = false;
    });
    return;
  }

  cargarColaboradoresBrigadas() {
    this.cargarColaboradoresFuente();
    this.cargarColaboradoresDestino();
  }

  ngOnInit() {
    this.cargarTablaBrigadas();
    this.cargarColaboradoresFuente();
    this.cargarTipoBrigada();
    this.cargarJefesBrigada();
  }

  cargarTablaBrigadas() {
    this.loadingBrigadas = true;
    this.brigadaService.getBrigadas().subscribe(
      data => {
        this.brigadas = data;
      },
      err => console.log(err)
    ).add(() => {
      this.loadingBrigadas = false;
    });
    return;
  }

  cargarJefesBrigada() {
    this.colaboradorService.getJefesBrigada().subscribe(
      data => {
        this.jefesBrigada = data;
        this.obtenerNombreJefe();
      },
      err => console.log(err)
    );
    return;
  }

  obtenerNombreJefe() {
    this.jefesBrigada.forEach(element => {
      if (element.nombres == null) {
        element.nombres = '';
      }
      if (element.apellido_paterno == null) {
        element.apellido_paterno = '';
      }
      if (element.apellido_materno == null) {
        element.apellido_materno = '';
      }
      element.nombres = element.nombres + ' ' + element.apellido_paterno + ' ' + element.apellido_materno;
    });
  }

  edit(rowData) {
    this.esNuevo = false;
    this.brigada = null;
    this.brigada = <Brigada>rowData;
    this.esVisiblePopUp = true;
  }

  nuevaFila() {
    this.esNuevo = true;
    this.brigada = new Brigada();
    this.esVisiblePopUp = true;
  }

  delete(rowData) {
    this.confirmationService.confirm({
      message: '¿Está seguro que desea eliminar la Brigada seleccionada?',
      accept: () => {
        this.brigada = rowData;
        this.brigadaService.eliminarBrigada(this.brigada).subscribe(
          response => {
            this.respuesta = response[0];
            this.cargarTablaBrigadas();
            this.brigada = new Brigada();
            this.esVisiblePopUp = false;
            this.toastService.verificarRespuesta(this.respuesta.estado, this.respuesta.mensaje);
          },
          err => console.log(err)
        );
      }
    });
  }

  save() {
    this.loadingBrigadas = true;
    this.brigadaService.registrarBrigada(this.brigada).subscribe(
      response => {
        this.respuesta = response[0];

        if (this.brigada.id_brigada === undefined) {
          this.brigadas.push(this.respuesta.objeto[0]);
        }

        this.brigada = this.respuesta.objeto[0];
        this.esVisiblePopUp = false;
        this.toastService.verificarRespuesta(this.respuesta.estado, this.respuesta.mensaje);
      },
      err => console.log(err)
    ).add(() => {
      this.loadingBrigadas = false;
    });
  }

  guardarBrigada() {
    this.loadingIntegrantes = true;
    this.brigadaService.registrarIntegrantesBrigada(this.brigadaSeleccionado, this.destinoColaboradores).subscribe(
      response => {
        this.respuesta = response[0];
        this.destinoColaboradores = this.respuesta.objeto;
        this.toastService.verificarRespuesta(this.respuesta.estado, this.respuesta.mensaje);
      },
      err => console.log(err)
    ).add(() => {
      this.loadingIntegrantes = false;
    });
  }
}
