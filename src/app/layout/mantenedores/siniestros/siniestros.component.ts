import { Component, OnInit } from '@angular/core';
import { Siniestro } from 'src/app/modelos/siniestro.model';
import { SiniestrosService } from 'src/app/services/siniestros.service';
import { trigger, state, style, transition, animate } from '@angular/animations';
import { RespuestaAPI } from 'src/app/modelos/respuestaAPI.model';
import { ToastService } from 'src/app/services/toast.service';
import { DialogService, ConfirmationService } from 'primeng/api';
import { Brigada } from 'src/app/modelos/brigada.model';
import { ElegirColaboradoresComponent } from '../../components/elegir-colaboradores/elegir-colaboradores.component';
import { NuevaActividadComponent } from '../../components/nueva-actividad/nueva-actividad.component';
import { Colaborador } from 'src/app/modelos/colaborador.model';
import { SeleccionarFechaComponent } from '../../components/seleccionar-fecha/seleccionar-fecha.component';
import { Rol } from 'src/app/modelos/rol.model';
import { BrigadaService } from 'src/app/services/brigada.service';
import { environment } from 'src/environments/environment';
import { Utils } from 'src/environments/utils';
import { ReportespdfService } from 'src/app/services/reportespdf.service';
import { ɵangular_packages_platform_browser_platform_browser_k } from '@angular/platform-browser';
import { RegistroHoras } from 'src/app/modelos/registroHoras';
import { element } from 'protractor';
import { HorasExtrasService } from 'src/app/services/horas-extras.service';
import { HorasExtrasComponent } from '../horas-extras/horas-extras.component';

@Component({
  selector: 'app-siniestros',
  templateUrl: './siniestros.component.html',
  animations: [
    trigger('rowExpansionTrigger', [
      state('void', style({
        transform: 'translateX(-10%)',
        opacity: 0
      })),
      state('active', style({
        transform: 'translateX(0)',
        opacity: 1
      })),
      transition('* <=> *', animate('400ms cubic-bezier(0.86, 0, 0.07, 1)'))
    ])
  ],
  styleUrls: ['./siniestros.component.scss'],
  providers: [
    DialogService,
    ConfirmationService
  ]
})
export class SiniestrosComponent implements OnInit {

  constructor(
    private siniestroService: SiniestrosService,
    private brigadaService: BrigadaService,
    private toastService: ToastService,
    private dialogService: DialogService,
    private confirmationService: ConfirmationService,
    private reportespdfService: ReportespdfService,
    private registroHorasService: HorasExtrasService) { }

  // Flags Boleanas
  esVisiblePopUp = false;
  esNuevo = false;

  // Fechas para el filtro de fechas
  fDesde: Date;
  fHasta: Date;

  // Flag para mostrar el Loading
  loadingActividades = false;

  siniestroM: Siniestro;
  siniestro: Siniestro;
  siniestroSeleccionado: Siniestro;
  siniestros: Siniestro[];

  // Para almacenar la respuesta de la API
  respuesta: RespuestaAPI;

  es: any;

  // Variable que guarda el ROL del usuario Logeado
  rol: Rol;

  // IdUsuarioLogeado
  idUsuarioLogeado: number;

  // Brigada Seleccionada -1 Equivale a todas las BRIGADAS
  brigadaSeleccionada: Brigada;
  listaBrigadasUsuario: Brigada[];

  // Columnas de la tabla de Colaborador.
  columnasTablaSiniestro: any = [
    { field: 'nro_foco', header: 'N° de foco, incendio o quema', type: 'readOnly'},
    // { field: 'id_siniestro', header: 'Id siniestro', type: 'readOnly' },
    // { field: 'editable', header: 'editable', type: 'input' },
    { field: 'id_brigada', header: 'Identificación del Recurso', type: 'brigada' },
    { field: 'codigo_actividad', header: 'Código actividad', type: 'readOnly'},
    { field: 'lugar_destino_cat', header: 'Lugar o Destino', type: 'catalogo'},
    // {field: 'centro_costo', header: 'Centro de costo'},
    { field: 'fecha_despacho', header: 'Despacho', type: 'calendar' },
    { field: 'fecha_salida', header: 'Salida o Comienzo', type: 'calendar' },
    { field: 'fecha_arribo', header: 'Arribo R-40', type: 'calendar' },
    { field: 'fecha_inicio', header: 'Inicio R-R24', type: 'calendar' },
    { field: 'fecha_termino', header: 'Termino R-24', type: 'calendar' },
    { field: 'fecha_regreso', header: 'Regreso', type: 'calendar' },
    { field: 'fecha_arribo_base', header: 'Arribo a base o fin de actividad', type: 'calendar' },
    { field: 'fuerza_combate', header: 'N° de Fuerza de combate', type: 'readOnly' }
  ];

  ngOnInit() {
    this.es = environment.locale_es;

    // this.es = {
    //   firstDayOfWeek: 1,
    //   dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
    //   dayNamesShort: ['dom', 'lun', 'mar', 'mié', 'jue', 'vie', 'sáb'],
    //   dayNamesMin: ['D', 'L', 'M', 'X', 'J', 'V', 'S'],
    //   monthNames: [
    //     'enero', 'febrero', 'marzo', 'abril', 'mayo',
    //     'junio', 'julio', 'agosto', 'septiembre', 'octubre',
    //     'noviembre', 'diciembre'
    //   ],
    //   monthNamesShort: ['ene', 'feb', 'mar', 'abr', 'may', 'jun', 'jul', 'ago', 'sep', 'oct', 'nov', 'dic'],
    //   today: 'Hoy',
    //   clear: 'Borrar'
    // };

    this.rol = JSON.parse( localStorage.getItem('colaborador') ).role[0];
    console.log(this.rol);
    this.idUsuarioLogeado = JSON.parse( localStorage.getItem('colaborador') ).id_colaborador;
    this.cargarBrigadasPorRolColaborador();

  }



  cargarTablaSiniestros() {
    this.loadingActividades = true;
    this.siniestroService.getSiniestrosPorBrigadaFechas(this.brigadaSeleccionada.id_brigada, this.fDesde, this.fHasta).subscribe(
      data => {
        this.siniestros = data;
        this.siniestros.forEach(function (e) {
          e.fecha_despacho != null ? e.fecha_despacho = new Date(e.fecha_despacho) : null;
          e.fecha_inicio != null ? e.fecha_inicio = new Date(e.fecha_inicio) : null;
          e.fecha_arribo != null ? e.fecha_arribo = new Date(e.fecha_arribo) : null;
          e.fecha_regreso != null ? e.fecha_regreso = new Date(e.fecha_regreso) : null;
          e.fecha_salida != null ? e.fecha_salida = new Date(e.fecha_salida) : null;
          e.fecha_termino != null ? e.fecha_termino = new Date(e.fecha_termino) : null;
          e.fecha_arribo_base != null ? e.fecha_arribo_base = new Date(e.fecha_arribo_base) : null;
        });
      },
      err => console.log(err)
    ).add(() => {
      this.loadingActividades = false;
    });
    return;
  }

  cargarBrigadasPorRolColaborador() {
    // listaBrigadasPorRol
    this.brigadaService.getBrigadasPorRolColaborador(this.rol, this.idUsuarioLogeado).subscribe(
      data => {
        if ( Array.isArray(data) ) {
          this.listaBrigadasUsuario = data;
          this.brigadaSeleccionada = this.listaBrigadasUsuario[0];
        } else {
          this.listaBrigadasUsuario = [];
          this.listaBrigadasUsuario.push(data);
          this.brigadaSeleccionada = data;
        }
        this.cargarTablaSiniestros();
      },
      err => console.log(err)
    );
  }

  editarFila(siniestro: Siniestro) {
    this.loadingActividades = true;
    // this.calcularHoras(siniestro);
    this.siniestroService.actualizarSiniestro(siniestro).subscribe(
      response => {
        this.respuesta = response[0];
        // if (siniestro.id_siniestro === undefined) {
        //   this.siniestros.push(this.respuesta.objeto[0]);
        // }
        this.toastService.verificarRespuesta(this.respuesta.estado, this.respuesta.mensaje);
      },
      err => console.log(err)
    ).add(() => {
      this.loadingActividades = false;
    });
  }

  delete(rowData) {
    this.confirmationService.confirm({
      message: '¿Está seguro que desea eliminar la actividad seleccionada?',
      accept: () => {
        this.siniestroM = rowData;
        this.siniestroService.eliminarSiniestro(this.siniestroM).subscribe(
          response => {
            this.respuesta = response[0];
            this.cargarTablaSiniestros();
            this.toastService.verificarRespuesta(this.respuesta.estado, this.respuesta.mensaje);
          },
          err => console.log(err)
        ).add(() => {
          this.loadingActividades = false;
        });
      }
    });
  }

  nuevaFila() {
    this.siniestro = new Siniestro();

    const ref = this.dialogService.open(NuevaActividadComponent, {
      header: 'Nueva actividad',
      width: 'auto',
      height: 'auto',
      contentStyle: { 'overflow': 'visible' },
      style: { 'overflow': 'visible' },
      data: { brigadaSeleccionada : this.brigadaSeleccionada }
    });

    ref.onClose.subscribe((datos: Siniestro) => {
        if (datos) {
          this.cargarTablaSiniestros();
        }
    });
  }

  toDate(dato: any) {
    return Utils.toDate(dato);
  }

  seleccionarFecha(siniestro: Siniestro, columna: string) {

    const parametros = {
      // Cargar la brigada del Jefe de Brigada
      fechaActual: siniestro[columna]
    };

    const ref = this.dialogService.open(SeleccionarFechaComponent, {
      header: 'Seleccione Fecha/Hora',
      width: 'auto',
      height: 'auto',
      closable: false,
      closeOnEscape: true,
      contentStyle: { 'overflow': 'auto' },
      style: { 'overflow': 'auto' },
      data: parametros
    });

    ref.onClose.subscribe(
      (fechaDeVuelta: Date) => {
        if (fechaDeVuelta) {

          if ( !this.validarFechas(siniestro, columna, fechaDeVuelta) ) {
            siniestro[columna] = null;
            return false;
          }
          if (siniestro['fecha_salida'] != null && siniestro['fecha_regreso'] != null && siniestro['fecha_regreso']) {
            let horaExtra: RegistroHoras = {};
            let horaExtraCR: RegistroHoras = {};
            horaExtra = Utils.obtenerHorasExtras(siniestro.fecha_salida, siniestro.fecha_regreso);
            horaExtraCR = Utils.obtenerHorasExtras(siniestro.fecha_salida, siniestro.fecha_arribo_base);
            horaExtra.id_brigada = siniestro.id_brigada;
            horaExtra.id_siniestro = siniestro.id_siniestro;

            horaExtra.horas_extras_sr = horaExtra.horas_extras;
            horaExtra.horas_extras_cr = horaExtraCR.horas_extras;
            horaExtra.t_horas_extras_sr = horaExtra.horas_extras * siniestro.fuerza_combate_real;
            horaExtra.t_horas_extras_cr = horaExtraCR.horas_extras * siniestro.fuerza_combate_real;
            this.registrarHoraExtra(horaExtra);
          }
          siniestro[columna] = fechaDeVuelta;
          this.editarFila(siniestro);

        } else {
          this.toastService.advertencia('No se ha seleccionado una fecha para guardar');
        }
      });

  }

  registrarHoraExtra(horaExtra: RegistroHoras) {
    this.registroHorasService.guardarHorasExtras(horaExtra).subscribe(
      data => {
        console.log(data);
      },
      err => console.log(err)
    );
  }

  guardarSiniestro() {
    this.loadingActividades = true;

    this.siniestroService.registrarSiniestro(this.siniestro).subscribe(
      response => {
        this.respuesta = response[0];

        if (this.siniestro.id_siniestro === undefined) {
          this.siniestros.push(this.respuesta.objeto[0]);
        }

        this.toastService.verificarRespuesta(this.respuesta.estado, this.respuesta.mensaje);
      }).add(() => {
        this.loadingActividades = false;
      });
  }

  // calcularHoras(siniestro: Siniestro){
  //   let arrayHoras: RegistroHoras[];

  //   arrayHoras = Utils.obtenerHorasExtras(siniestro.fecha_salida,siniestro.fecha_regreso);

  //   arrayHoras.forEach(element => {
  //     element.id_brigada = siniestro.id_brigada;
  //     element.id_siniestro = siniestro.id_siniestro;

  //     element.horas_extras_sr = element.horas_extras;
  //     element.t_horas_extras_sr = element.horas_extras * siniestro.fuerza_combate_real;

  //     console.log('Horas sin retorno');
  //     console.log(element);
  //   });
  // }


  editarColaboradores(siniestro: Siniestro) {

    // Brigada Dummy
    const brigadaSeleccionada: Brigada = {
      id_brigada: siniestro.id_brigada
    };

    const siniestroSeleccionado: Siniestro = {
      id_siniestro: siniestro.id_siniestro
    };

    const parametros = {
      // Cargar la brigada del Jefe de Brigada
      Brigada: brigadaSeleccionada,
      Siniestro: siniestroSeleccionado
    };

    const ref = this.dialogService.open(ElegirColaboradoresComponent, {
      header: 'Elegir Participantes',
      width: 'auto',
      height: 'auto',
      closable: false,
      closeOnEscape: false,
      contentStyle: { 'overflow': 'auto' },
      style: { 'overflow': 'auto' },
      data: parametros
    });

    ref.onClose.subscribe(
      (datos: any) => {
        siniestro.colaboradores = datos;
        this.siniestro = siniestro;
        this.guardarSiniestro();
        // this.editarFila(this.siniestro);
      });
  }

  guardarColaboradoresBrigada(listaColaboradores: Colaborador[]) { }

  cerrar(siniestro: Siniestro) {
    this.confirmationService.confirm({
      message: '¿Está seguro que desea cerrar la actividad seleccionada?',
      accept: () => {
        siniestro.editable = 0;
        this.editarFila(siniestro);
        /*
        this.siniestroService.actualizarSiniestro(siniestro).subscribe(
          response => {
            this.respuesta = response[0];
            this.cargarTablaSiniestros();
            this.toastService.verificarRespuesta(this.respuesta.estado, this.respuesta.mensaje);
          },
          err => console.log(err)
        ).add(() => {
          this.loadingActividades = false;
        });
        */
      }
    });
  }

  exportToPDF() {
    this.reportespdfService.getInformeActividades1(this.brigadaSeleccionada.id_brigada, this.fDesde, this.fHasta);
  }

  puedeEditar(editable: any) {
    // Si es administrador siempre puede editar.
    if ( this.rol.id_rol == 1 ) {
      return true;
    }

    // Si es supervisor también.
    if ( this.rol.id_rol == 2 ) {
      return true;
    }

    // Si no es, entonces depende de la variable "Editable" de la tabla.
    if ( editable == 1 ) {
      return true;
    } else {
      return false;
    }
  }

  validarFechas(siniestro: Siniestro, columna: string, fechaDeVuelta: Date) {
    console.log(columna);
    let result = false;
    // return true;

    try {
      switch (columna) {
        case 'fecha_despacho':
          result = true;
        break;
        case 'fecha_salida':
          if (siniestro['fecha_despacho'].getTime() <= fechaDeVuelta.getTime()) {
            result = true;
          } else {
            this.toastService.advertencia('La fecha/hora de Salida no puede ser menor que la de Despacho');
            return false;
          }
        break;
        case 'fecha_arribo':
            if (siniestro['fecha_salida'].getTime() <= fechaDeVuelta.getTime()) {
              result = true;
            } else {
              this.toastService.advertencia('La fecha/hora de Arribo no puede ser menor que la de Salida');
              return false;
            }
        break;
        case 'fecha_inicio':
            if (siniestro['fecha_arribo'].getTime() <= fechaDeVuelta.getTime()) {
              result = true;
            } else {
              this.toastService.advertencia('La fecha/hora de Inicio no puede ser menor que la de Arribo');
              return false;
            }
        break;
        case 'fecha_termino':
            if (siniestro['fecha_inicio'].getTime() <= fechaDeVuelta.getTime()) {
              result = true;
            } else {
              this.toastService.advertencia('La fecha/hora de Término no puede ser menor que la de Inicio');
              return false;
            }
        break;
        case 'fecha_regreso':
            if (siniestro['fecha_termino'].getTime() <= fechaDeVuelta.getTime()) {
              result = true;
            } else {
              this.toastService.advertencia('La fecha/hora de Regreso no puede ser menor que la de Termino');
              return false;
            }
        break;
        case 'fecha_arribo_base':
            if (siniestro['fecha_regreso'].getTime() <= fechaDeVuelta.getTime()) {
              result = true;
            } else {
              this.toastService.advertencia('La fecha/hora de Arribo a Base no puede ser menor que la de Regreso');
              return false;
            }
        break;
        default:
            return false;
        break;
      }
    } catch (error) {
      this.toastService.advertencia('Es necesario seleccionar la fecha anterior antes de esta.');
    }

    return result;
  }
}
