import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import * as moment from 'moment';
import { environment } from 'src/environments/environment';
import { BrigadaService } from 'src/app/services/brigada.service';
import { Brigada } from 'src/app/modelos/brigada.model';
import { Rol } from 'src/app/modelos/rol.model';
import { Colaborador } from 'src/app/modelos/colaborador.model';
import { ColaboradorService } from 'src/app/services/colaborador.service';
import { HorasExtrasService } from 'src/app/services/horas-extras.service';
import { RegistroHoras } from 'src/app/modelos/registroHoras';
import { ToastService } from 'src/app/services/toast.service';
import { ReportespdfService } from 'src/app/services/reportespdf.service';

@Component({
  selector: 'app-horas-extras-brigada',
  templateUrl: './horas-extras-brigada.component.html',
  styleUrls: ['./horas-extras-brigada.component.scss']
})
export class HorasExtrasBrigadaComponent implements OnInit {

  columnasTablaHorasExtras: any = [
    // {field: 'nombres', header: 'Nombre/Apellido'},
    // {field: 'cargo', header: 'Cargo'}
  ];

  columnasEstaticas: any = [
    {field: 'nombres', header: 'Nombre/Apellido'},
    {field: 'cargo', header: 'Cargo'}
  ];
  columnasTablasHorasExtrasFinal: any = [];
  adicional: any;
  dias: any = [];
  es: any;

  // Fechas para el filtro de fechas
  fDesde: Date = new Date();
  fHasta: Date = new Date();

  listaBrigadasUsuario: Brigada[] = [];
  brigadaSeleccionada: Brigada;

  // Variable que guarda el ROL del usuario Logeado
  rol: Rol;
  // IdUsuarioLogeado
  idUsuarioLogeado: number;

  colaboradoresBrigada: Colaborador[];
  colaboraresBrigadaHoras = [];
  // colaboraresBrigadaHoras = [
  //   {
  //     'apellido_paterno': 'a0sjdasd',
  //     'apellido_materno': 'asjdjlad',
  //     'cargo': {
  //               'id_catalogo': '10',
  //               'valor': 'SUPERVISOR'
  //     },
  //     'id_colaborador': '42',
  //     'nombres': 'kjassd kajhsdjkasd',
  //     'rut': '77777777-7',
  //     '06-11-2019': '34',
  //     '07-11-2019': '32'
  //   },
  //   {
  //     'apellido_paterno': 'asdasd',
  //     'apellido_materno': 'asdasd',
  //     'cargo': {
  //               'id_catalogo': '10',
  //               'valor': 'SUPERVISOR'
  //     },
  //     'id_colaborador': '42',
  //     'nombres': 'kjassd kajhsdjkasd',
  //     'rut': '77777777-7',
  //     '06-11-2019': '',
  //     '10-11-2019': '34',
  //     '11-11-2019': '32',
  //     '12-11-2019': '32',
  //   },

  // ];

  constructor(private brigadaService: BrigadaService,
              private colaboradorService: ColaboradorService,
              private registroHorasService: HorasExtrasService,
              private toastService: ToastService,
              private reporteService: ReportespdfService
              ) { }


  ngOnInit() {
    this.fDesde.setDate(this.fDesde.getDay() - 10);
    this.rol = JSON.parse( localStorage.getItem('colaborador') ).role[0];
    this.idUsuarioLogeado = JSON.parse( localStorage.getItem('colaborador') ).id_colaborador;

    this.es = environment.locale_es;
    this.cargarBrigadasPorRolColaborador();
    // this.cargarColaboradoresPorBrigada();
    // this.cargarColaboradoresBrigada(this.brigadaSeleccionada.id_brigada);
    this.pintarTabla();
  }

  pintarTabla() {
    this.columnasTablasHorasExtrasFinal = [];
    this.columnasTablaHorasExtras.forEach( element => {
      this.columnasTablasHorasExtrasFinal.push(element);
    });
    this.dias = this.diasEntre(this.fDesde, this.fHasta);
    this.dias.forEach(element => {
      this.adicional = {field: element.format('YYYY-MM-DD'), header: element.date(), width: '50px'};
      console.log(this.adicional);
      this.columnasTablasHorasExtrasFinal.push(this.adicional);
    });
  }

  exportToExcel() {
    this.reporteService.getInformeHorasExtraBrigada(this.brigadaSeleccionada.id_brigada, this.fDesde, this.fHasta );
  }

  cargarBrigadasPorRolColaborador() {
    // listaBrigadasPorRol
    this.brigadaService.getBrigadasPorRolColaborador(this.rol, this.idUsuarioLogeado).subscribe(
      data => {
        if ( Array.isArray(data) ) {
          this.listaBrigadasUsuario = data;
          this.brigadaSeleccionada = this.listaBrigadasUsuario[0];
        } else {
          this.listaBrigadasUsuario = [];
          this.listaBrigadasUsuario.push(data);
          this.brigadaSeleccionada = data;
        }
        console.log(this.listaBrigadasUsuario);
        console.log(this.brigadaSeleccionada.id_brigada);
        this.cargarColaboradoresBrigada(this.brigadaSeleccionada.id_brigada, this.fDesde, this.fHasta);
      },
      err => console.log(err)
    );
  }

  cargarColaboradoresPorBrigada() {
    console.log(this.brigadaSeleccionada);
    this.colaboradorService.colaboradorBrigada(this.brigadaSeleccionada.id_brigada).subscribe(
      data => {
        // this.colaboradoresBrigada = data;
        console.log(data);
      }
    );
  }

  cargarColaboradoresBrigada(idBrigada, fDesde, fHasta) {
    if ( fDesde == null || fDesde == undefined || fHasta == null || fHasta == undefined) {
      console.log(fDesde, fHasta);
      this.toastService.advertencia('Es necesario seleccionar las fechas a consultar');
      return true;
    }

    fDesde = moment(fDesde).format('YYYY-MM-DD');
    fHasta = moment(fHasta).format('YYYY-MM-DD');
    this.registroHorasService.obtenerHorasExtrasBrigada(idBrigada, fDesde, fHasta).subscribe(
      data => {
        this.colaboraresBrigadaHoras = data;
        console.log('============== this.colaboraresBrigadaHoras ==============');
        console.log(this.colaboraresBrigadaHoras);
        console.log('============== this.colaboraresBrigadaHoras ==============');
        this.pintarTabla();
      }
    );
  }

  diasEntre(from, to) {
    const fromDate = moment(new Date(from)).startOf('day');
    const toDate = moment(new Date(to)).endOf('day');

    const span = moment.duration(toDate.diff(fromDate)).asDays();
    const days = [];
    for (let i = 0; i <= span; i++) {
      days.push(moment(fromDate).add(i, 'day').startOf('day'));
    }
    return days;
  }

}
