import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { HorasExtrasBrigadaComponent } from './horas-extras-brigada.component';

describe('HorasExtrasBrigadaComponent', () => {
  let component: HorasExtrasBrigadaComponent;
  let fixture: ComponentFixture<HorasExtrasBrigadaComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ HorasExtrasBrigadaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HorasExtrasBrigadaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
