import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Brigada } from 'src/app/modelos/brigada.model';
import { SiniestrosService } from 'src/app/services/siniestros.service';
import { ToastService } from 'src/app/services/toast.service';
import { RespuestaAPI } from 'src/app/modelos/respuestaAPI.model';
import { Siniestro } from 'src/app/modelos/siniestro.model';
import * as moment from 'moment';
import { BrigadaService } from 'src/app/services/brigada.service';
import { Rol } from 'src/app/modelos/rol.model';
import { ReportespdfService } from 'src/app/services/reportespdf.service';
import { RegistroHoras } from 'src/app/modelos/registroHoras';
import { HorasExtrasService } from 'src/app/services/horas-extras.service';
import { analyzeAndValidateNgModules } from '@angular/compiler';
import { Utils } from 'src/environments/utils';

@Component({
  selector: 'app-horas-extras',
  templateUrl: './horas-extras.component.html',
  styleUrls: ['./horas-extras.component.scss']
})
export class HorasExtrasComponent implements OnInit {

  constructor(private siniestroService: SiniestrosService,
              private brigadaService: BrigadaService,
              private registroHorasService: HorasExtrasService,
              private toastService: ToastService,
              private reportespdfService: ReportespdfService) { }
  es: any;
  actividadesPorBrigada: Siniestro[];
  respuesta: RespuestaAPI;

  listaBrigadasUsuario: Brigada[] = [];
  brigadaSeleccionada: Brigada;

  // Fechas para el filtro de fechas
  fDesde: Date;
  fHasta: Date;

  // Variable que guarda el ROL del usuario Logeado
  rol: Rol;
  // IdUsuarioLogeado
  idUsuarioLogeado: number;

  horaExtra: RegistroHoras = {};
  horas: any;

  columnasTablaHorasExtras: any = [
    {field: 'id_siniestro', header: 'ID Siniestro'},
    {field: 'nro_foco', header: 'Número de incendio'},
    {field: 'fuerza_combate', header: 'Número de personas'},
    {field: 'codigo_actividad', header: 'Actividad'},
    {field: 'fecha_salida', header: 'Salida'},
    {field: 'fecha_regreso', header: 'Regreso'},
    {field: 'fecha_arribo_base', header: 'Arribo a base'},
    {field: 'horas_extras_sr', header: 'Horas Extras SR'},
    {field: 'horas_extras_cr', header: 'Horas Extras CR'},
    {field: 't_horas_extras_sr', header: 'Total Horas SR'},
    {field: 't_horas_extras_cr', header: 'Total Horas CR'},
    {field: 'lugar_destino_cat', header: 'Lugar'},
    {field: 'observacion', header: 'Observación'},
  ];

    ngOnInit() {
        this.es = environment.locale_es;
        this.rol = JSON.parse( localStorage.getItem('colaborador') ).role[0];
        this.idUsuarioLogeado = JSON.parse( localStorage.getItem('colaborador') ).id_colaborador;
        this.cargarBrigadasPorRolColaborador();
        // this.obtenerHorasExtras('2019-10-14 08:00:00', '2019-10-15 12:00:00');
    }


    cargarBrigadasPorRolColaborador() {
      // listaBrigadasPorRol
      this.brigadaService.getBrigadasPorRolColaborador(this.rol, this.idUsuarioLogeado).subscribe(
        data => {
          if ( Array.isArray(data) ) {
            this.listaBrigadasUsuario = data;
            this.brigadaSeleccionada = this.listaBrigadasUsuario[0];
          } else {
            this.listaBrigadasUsuario = [];
            this.listaBrigadasUsuario.push(data);
            this.brigadaSeleccionada = data;
          }
          console.log(this.listaBrigadasUsuario);
          console.log(this.brigadaSeleccionada.id_brigada);
          this.cargarActividadesPorBrigada();
        },
        err => console.log(err)
        );
    }

    registrarHoraExtra(horaExtra: RegistroHoras) {
      this.registroHorasService.guardarHorasExtras(horaExtra).subscribe(
        data => {
          console.log(data);
        },
        err => console.log(err)
      );
    }

    // obtenerHorasExtras(horaInicio: string, horaFin: string){
    //   const jornadas = [
    //     {
    //       inicioJornadaHora: 11,
    //       inicioJornadaMinuto: 0,
    //       finJornadaHora: 12,
    //       finJornadaMinuto: 0
    //     },
    //     {
    //       inicioJornadaHora: 13,
    //       inicioJornadaMinuto: 0,
    //       finJornadaHora: 19,
    //       finJornadaMinuto: 30
    //     }
    //   ];
    //   let horasExtras = 0;
    //   const inicioActividad = moment(horaInicio, 'YYYY-MM-DD HH:mm');
    //   const finActividad = moment(horaFin, 'YYYY-MM-DD HH:mm');

    //   if (inicioActividad.isBefore(finActividad)) {
    //     // console.log('hora de inicio menor a la hora de fin');
    //     let dia_actual = inicioActividad.clone();
    //     let diaActualComparar = inicioActividad.clone();

    //     while (dia_actual.isSameOrBefore(finActividad)) {
    //       // console.log('******************** Inicio dia ********************');
    //       let horaInicioActual = dia_actual.clone();
    //       let horaFinActual = null;
    //       diaActualComparar = dia_actual;
    //       // console.log('horaInicioActual: ' + horaInicioActual.format('YYYY-MM-DD HH:mm'));
    //       // Se comprueba si el fin de la actividad está dentro del día, si no lo está se valoriza la hora de fin de la
    //       // actividad con la última hora del día
    //       if (finActividad.isSameOrBefore(diaActualComparar.set({ 'hour': 23, 'minute': 59, 'second': 59 }))) {
    //         horaFinActual = finActividad.clone();
    //       } else {
    //         horaFinActual = diaActualComparar.set({ 'hour': 23, 'minute': 59, 'second': 59 }).clone();
    //       }
    //       // console.log('Día actual: ' + dia_actual.format('YYYY-MM-DD HH:mm'));
    //       // console.log(horaInicioActual.format('YYYY-MM-DD HH:mm') + ' / ' + horaFinActual.format('YYYY-MM-DD HH:mm'));
    //       let horaInicioTemp = horaInicioActual.clone();
    //       let horaFinTemp = horaFinActual.clone();
    //       // Variable para marcar si una actividad terminó antes de recorrer todas las jornadas laborales.
    //       let finActividadFlag = false;

    //       jornadas.forEach(function (element, i) {
    //         // console.log('======= JORNADA ' + i + ' =============');
    //         let inicioJornada = moment(horaInicioActual).set({
    //           'hour': element.inicioJornadaHora, 'minute': element.inicioJornadaMinuto, 'second': 0
    //         }).clone();
    //         let finJornada = moment(horaFinActual).set({
    //           'hour': element.finJornadaHora, 'minute': element.finJornadaMinuto, 'second': 0
    //         }).clone();


    //         if (horaInicioTemp.isSameOrAfter(inicioJornada)) {
    //           // console.log('no se suman horas extras [inicioActividad >= inicioJornada]');
    //         } else {
    //           // Comprobar si la hora de fin de actividad es menor al inicio de la jornada, entonces
    //           // calcular la diferencia entre inicio de la actividad y el fin de la actividad.
    //           // Si se cumple la condición significa que la actividad ya terminó para este día y no debe continuar con
    //           // la siguiente jornada.
    //           if (horaFinTemp.isSameOrBefore(inicioJornada) && horaInicioTemp.isSameOrBefore(inicioJornada) && !finActividadFlag) {
    //             // console.log('Hora fin de la actividad menor que la hora de inicio de jornada: Horas extras: ' + horaFinTemp.diff(horaInicioTemp, 'seconds'));
    //             horasExtras += horaFinTemp.diff(horaInicioTemp, 'seconds');
    //             finActividadFlag = true;
    //           } else if (!finActividadFlag) {
    //             // console.log('Se suman horas extras [else - inicioActividad >= inicioJornada]');
    //             // console.log(inicioJornada.diff(horaInicioTemp, 'seconds'));
    //             horasExtras += inicioJornada.diff(horaInicioTemp, 'seconds');
    //           }
    //         }

    //         if (horaFinTemp.isSameOrBefore(finJornada)) {
    //           // console.log('no se suman horas extras [finActividad <= finJornada]');
    //         } else {
    //           if (jornadas.length == i + 1) {
    //             // console.log('Se suman horas extras [finActividad <= finJornada]');
    //             if (horaInicioTemp.isSameOrBefore(finJornada)) {
    //               // console.log(horaFinTemp.diff(finJornada, 'seconds'));
    //               horasExtras += horaFinTemp.diff(finJornada, 'seconds');
    //             } else {
    //               // console.log(horaFinTemp.diff(horaInicioTemp, 'seconds'));
    //               horasExtras += horaFinTemp.diff(horaInicioTemp, 'seconds');
    //             }
    //           } else if (horaInicioTemp.isSameOrBefore(finJornada)) {
    //             horaInicioTemp = finJornada.clone();
    //           }
    //         }
    //         if (horaInicioTemp.isSameOrBefore(finJornada)) {
    //           horaInicioTemp = finJornada.clone();
    //         }
    //         // console.log('======= FIN JORNADA =============');
    //       });

    //       this.horaExtra.id_brigada = 3;
    //       this.horaExtra.id_siniestro = 107;
    //       this.horaExtra.total_horas_extras = horasExtras;
    //       this.horaExtra.fecha_horas_extras = dia_actual.toDate();

    //       //this.registrarHoraExtra(this.horaExtra);
    //       // console.log("Llamando a la función que graba las horas extras por día y brigada");
    //       this.horas = (horasExtras / 3600).toFixed(2);
    //       console.log(this.horas);
    //       dia_actual.add({ days: 1 }).set({ 'hour': 0, 'minute': 0, 'second': 0 });
    //       // console.log('Dia Actual: ' + dia_actual.format('DD-MM-YYYY HH:mm'));
    //       // console.log('Horas extras dia: ' + horasExtras);
    //       // console.log('******************** Fin dia ********************');
    //     }
    //   }
    //   // return Math.round(horasExtras / 60);
    //   return (horasExtras / 3600).toFixed(2);
    // }

    cargarActividadesPorBrigada() {
      const obtener = null;
      // console.log(this.brigadaSeleccionada.id_brigada);
      this.siniestroService.getSiniestrosPorBrigadaFechas(this.brigadaSeleccionada.id_brigada, this.fDesde, this.fHasta).subscribe(
        response => {
          // console.log(response);
          this.actividadesPorBrigada = response;
          this.actividadesPorBrigada = this.calcularHoras(this.actividadesPorBrigada);

          this.actividadesPorBrigada.forEach(function (e) {
            e.fecha_arribo != null ? e.fecha_arribo =  this.toDate(e.fecha_arribo) : null;
            e.fecha_regreso != null ? e.fecha_regreso = this.toDate(e.fecha_regreso) : null;
            e.fecha_salida != null ? e.fecha_salida = this.toDate(e.fecha_salida) : null;
            e.fecha_arribo_base != null ? e.fecha_arribo_base = this.toDate(e.fecha_arribo_base) : null;
          });


          // this.rolesAct = this.respuesta.objeto;
          // this.toastService.verificarRespuesta(this.respuesta.estado, this.respuesta.mensaje);
        },
        err => console.log(err)
      );
    }
    calcularHoras(actividades: Siniestro[]) {
      const esto = this;
      const horaExtra: RegistroHoras = {};
      let horaExtraCR: RegistroHoras = {};
      let horas: number;
      let actividadTemp: Siniestro;
      const actividadesTemp: Siniestro[] = [];

      actividades.forEach(function(value) {
        let horaExtra: RegistroHoras = {};
        actividadTemp = value;
        // if(actividadTemp.id_siniestro == 147){
        horaExtra = Utils.obtenerHorasExtras(value.fecha_salida, value.fecha_regreso);
        horaExtraCR = Utils.obtenerHorasExtras(value.fecha_salida, value.fecha_arribo_base);
        horaExtra.id_brigada = value.id_brigada;
        horaExtra.id_siniestro = value.id_siniestro;

        horaExtra.horas_extras_sr = horaExtra.horas_extras;
        horaExtra.horas_extras_cr = horaExtraCR.horas_extras;
        horaExtra.t_horas_extras_sr = horaExtra.horas_extras * value.fuerza_combate_real;
        horaExtra.t_horas_extras_cr = horaExtraCR.horas_extras * value.fuerza_combate_real;

        actividadTemp.horas_extras_sr = horaExtra.horas_extras;
        actividadTemp.t_horas_extras_sr = horaExtra.t_horas_extras_sr;
        actividadTemp.horas_extras_cr = horaExtraCR.horas_extras;
        actividadTemp.t_horas_extras_cr = horaExtra.t_horas_extras_cr;

        actividadesTemp.push(actividadTemp);
        esto.registrarHoraExtra(horaExtra);
      // }
      } );
      return actividadesTemp;
    }

    // obtenerHorasExtras(horaInicio: Date, horaFin: Date, horasExtras: RegistroHoras){
    //   return Utils.obtenerHorasExtras(horaInicio, horaFin).horas_extras_sr;
    // }

    exportToPDF() {
      this.reportespdfService.getInformeHorasExtra(this.brigadaSeleccionada.id_brigada, this.fDesde, this.fHasta);
    }

    exportToEXCEL() {
      this.reportespdfService.getInformeHorasExtraExcel(this.brigadaSeleccionada.id_brigada, this.fDesde, this.fHasta);
    }

    toDate(dato: any) {
      return Utils.toDate(dato);
    }
}
