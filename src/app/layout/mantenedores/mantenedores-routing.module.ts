import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ColaboradoresComponent } from './colaboradores/colaboradores.component';
import { BrigadasComponent } from './brigadas/brigadas.component';
import { RolesComponent } from './roles/roles.component';
import { RoleGuard } from 'src/app/services/role-guard.service';
import { SiniestrosComponent } from './siniestros/siniestros.component';
import { AsistenciaComponent } from './asistencia/asistencia.component';
import { SupervisorComponent } from './supervisor/supervisor.component';
import { HorasExtrasComponent } from './horas-extras/horas-extras.component';
import { HorasExtrasBrigadaComponent } from './horas-extras-brigada/horas-extras-brigada.component';

const routes: Routes = [
  { path: 'colaboradores', component: ColaboradoresComponent},
  { path: 'brigadas', component: BrigadasComponent},
  { path: 'roles', component: RolesComponent},
  { path: 'siniestros', component: SiniestrosComponent},
  { path: 'asistencia', component: AsistenciaComponent},
  { path: 'supervisores', component: SupervisorComponent},
  { path: 'horas-extras', component: HorasExtrasComponent},
  { path: 'horas-extras-brigada', component: HorasExtrasBrigadaComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MantenedoresRoutingModule { }
