import { Component, OnInit } from '@angular/core';
import { Colaborador } from 'src/app/modelos/colaborador.model';
import { ColaboradorService } from 'src/app/services/colaborador.service';
import { ToastService } from '../../../services/toast.service';
import { RespuestaAPI } from 'src/app/modelos/respuestaAPI.model';
import { CatalogoService } from 'src/app/services/catalogo.service';
import { Catalogo } from 'src/app/modelos/catalogo.model';
import * as CryptoJS from 'crypto-js';
import { Rol } from 'src/app/modelos/rol.model';
import { RolService } from 'src/app/services/rol.service';
import { ColaboradorRol } from 'src/app/modelos/colaboradorRol.model';
import { Subscription } from 'rxjs';
import { ConfirmationService } from 'primeng/api';

@Component({
  selector: 'app-colaboradores',
  templateUrl: './colaboradores.component.html',
  styleUrls: ['./colaboradores.component.scss'],
  providers: [
    ConfirmationService
  ]
})
export class ColaboradoresComponent implements OnInit {

  constructor (
    private rolService: RolService,
    private colaboradorService: ColaboradorService,
    private catalogosService: CatalogoService,
    private toastService: ToastService,
    private confirmationService: ConfirmationService ) {}

  // Flags Boleanas
  esVisiblePopUp = false;
  esNuevo = false;

  // Colaboradores.
  colaboradorM: Colaborador;
  colaboradores: Colaborador[];
  colaboradorSeleccionado: Colaborador;
  catalogoCargos: Catalogo[];
  catalogoCargoSelecionado: Catalogo;
  cEditable: Colaborador;
  colaboradorRoles: ColaboradorRol[];

  respuesta: RespuestaAPI;
  rolesAct: Rol[];
  roles: Rol[];

  // Para los loadings
  loadingTabla = false;
  loadingRoles = false;

  // Variable Dummy para guardar los datos de los select.
  selectDummy: any = [
    {value: 1, label: 'VALOR 1'},
    {value: 2, label: 'VALOR 2'}
  ];

  // Columnas de la tabla de Colaborador.
  columnasTablaColaborador: any = [
    {field: 'rut', header: 'RUT'},
    {field: 'nombres', header: 'Nombres'},
    {field: 'apellido_paterno', header: 'Apellido P.'},
    {field: 'apellido_materno', header: 'Apellido M.'},
    {field: 'cargo', header: 'Cargo'},
    {field: 'es_usuario', header: 'Es usuario'},
  ];

  ngOnInit() {
    this.cargarTablaColaboradores();
    this.cargarCargos();
  }

  cargarTablaColaboradores() {
    this.loadingTabla = true;
    this.colaboradorService.getColaboradores().subscribe(
      data => {
        this.colaboradores = data;
        console.log(this.colaboradores);
      },
      err => console.log(err)
      ).add(() => {
        this.loadingTabla = false;
      });
      return;
  }

  cargarCargos() {
    return this.catalogosService.getCatalogo('CARGOS').subscribe(
      data => {
        this.catalogoCargos = data;
        this.catalogoCargoSelecionado = data[0];
      },
        err => console.log(err)
      );
  }

  edit(rowData) {
    this.esNuevo = false;
    this.colaboradorM = rowData;
    this.esVisiblePopUp = true;
  }

  nuevaFila() {
    this.esNuevo = true;
    this.colaboradorM = new Colaborador();
    this.esVisiblePopUp = true;
  }

  delete(rowData) {
    this.confirmationService.confirm({
      message: '¿Está seguro que desea eliminar el Colaborador seleccionado?',
      accept: () => {
        this.loadingTabla = true;
        this.colaboradorM = rowData;
        this.colaboradorService.eliminarColaborador(this.colaboradorM).subscribe(
          response => {
            this.respuesta = response[0];
            this.cargarTablaColaboradores();
            this.colaboradorM = new Colaborador();
            this.esVisiblePopUp = false;
            this.toastService.verificarRespuesta(this.respuesta.estado, this.respuesta.mensaje);
          },
          err => console.log(err)
        ).add(() => {
          this.loadingTabla = false;
        });
      }
    });
  }

  save() {
    // console.log('es MD5: ' + this.comprobarMD5(this.colaboradorM.contrasena));
    if (!this.comprobarMD5(this.colaboradorM.contrasena)) {
      this.colaboradorM.contrasena = CryptoJS.MD5(this.colaboradorM.contrasena).toString();
    }

    console.log(this.colaboradorM);

    this.colaboradorM.cargo = this.catalogoCargoSelecionado;
    this.loadingTabla = true;
    this.colaboradorService.registrarColaborador(this.colaboradorM).subscribe(
      response => {
        this.respuesta = response[0];

        if ( this.colaboradorM.id_colaborador === undefined) {
          this.colaboradores.push( this.respuesta.objeto[0] );
        }

        this.colaboradorM = this.respuesta.objeto[0];
        this.esVisiblePopUp = false;
        this.toastService.verificarRespuesta(this.respuesta.estado, this.respuesta.mensaje);
      },
      err => console.log(err)
    ).add(() => {
      this.loadingTabla = false;
    });
  }

  // Funcion para comprobar si un string es un hash MD5
  comprobarMD5(md5: string) {
    const regexp = new RegExp('^[a-f0-9]{32}$');
    return regexp.test(md5);
  }

  rolesActuales() {
    this.loadingRoles = true;
    this.rolService.getRolesbyRut(this.colaboradorSeleccionado.rut).subscribe(
      response => {
        this.rolesAct = response;
      }
    ).add(() => {
      this.loadingRoles = false;
    });

    this.rolService.getRolessinAsignar(this.colaboradorSeleccionado.rut).subscribe(
      response => {
        this.roles = response;
      }
    ).add(() => {
      this.loadingRoles = false;
    });
  }

  guardarRolesUsuario(rol: Rol, colaboradorSeleccionado: Colaborador) {
    const col = this;
    col.colaboradorRoles = [];
    Object.values(rol).forEach(function(element) {
      const colaboradorRolTemp = new ColaboradorRol();
      colaboradorRolTemp.id_colaborador = colaboradorSeleccionado.id_colaborador;
      colaboradorRolTemp.id_rol = element.id_rol;
      col.colaboradorRoles.push(colaboradorRolTemp);
    });


    this.loadingRoles = true;
    this.rolService.guardarRolesUsuario(this.colaboradorRoles).subscribe(
      response => {
        console.log(response);
        this.respuesta = response;
        // this.rolesAct = this.respuesta.objeto;
        this.toastService.verificarRespuesta(this.respuesta.estado, this.respuesta.mensaje);
      },
      err => console.log(err)
    ).add(() => {
      this.loadingRoles = false;
    });
  }

  onRowSelect(event) {
    console.log(this.colaboradorSeleccionado);
    // tslint:disable-next-line:triple-equals
    if (this.colaboradorSeleccionado.es_usuario == 1) {
      console.log('entrando al if');
      this.rolesActuales();
    }
  }

  oneToTarget() {
    if ( this.rolesAct.length > 1 ) {
      console.log('oneToTarget');
      this.toastService.informacion('Sólo puede tener un rol');
      // this.roles.push( this.rolesAct.pop() );
    }
    while ( this.rolesAct.length > 1) {
      this.roles.push( this.rolesAct.pop() );
    }
  }

  allToTarget() {
    console.log('allToTarget');
    if ( this.rolesAct.length > 1 ) {
      this.toastService.informacion('Sólo puede tener un rol');
    }

    while ( this.rolesAct.length > 1) {
      this.roles.push( this.rolesAct.pop() );
    }
  }
}
