import { Component, OnInit } from '@angular/core';
import { AsistenciaService } from 'src/app/services/asistencia.service';
import { CatalogoService } from 'src/app/services/catalogo.service';
import { BrigadaService } from 'src/app/services/brigada.service';
import { ToastService } from 'src/app/services/toast.service';
import { Colaborador } from 'src/app/modelos/colaborador.model';
import { Brigada } from 'src/app/modelos/brigada.model';
import { Catalogo } from 'src/app/modelos/catalogo.model';
import { DiaService } from 'src/app/services/dia.service';
import { Dia } from 'src/app/modelos/dia.model';
import { RespuestaAPI } from 'src/app/modelos/respuestaAPI.model';
import { environment } from 'src/environments/environment';
import { Rol } from 'src/app/modelos/rol.model';
import { ReportespdfService } from 'src/app/services/reportespdf.service';

@Component({
  selector: 'app-asistencia',
  templateUrl: './asistencia.component.html',
  styleUrls: ['./asistencia.component.scss']
})
export class AsistenciaComponent implements OnInit {

  // Brigada Seleccionada -1 Equivale a todas las BRIGADAS
  brigadaSeleccionada: Brigada;
  listaBrigadasUsuario: Brigada[] = [];

  // Variable que guarda el ROL del usuario Logeado
  rol: Rol;

  // IdUsuarioLogeado
  idUsuarioLogeado: number;

  // Variable para configurar el calendario.
  es: any = environment.locale_es;

  // Para el loading de la tabla de asistencias.
  loadingAsistencia = false;

  // Valores tabla.
  valAsistencias: any[];

  // Valores del DROP de asistencia.
  valoresAsistencia: Catalogo[];

  // Para guardar el valor de la asistencia seleccionada.
  asistenciaSeleccionada: Catalogo;

  // Columnas de la tabla de Asistencia.
  columnasTablaAsistencia: any = [
    { field: 'NOMBRES', header: 'Colaborador' }
  ];

  // Fecha Seleccionada.
  fechaSeleccionada: Date = null;

  userCol: Colaborador;

  // Respuesta de la API
  respuesta: RespuestaAPI;

  colaboradoresPorBrigada: Colaborador[];

  // Variable de TEST
  flagVisible = false;

  constructor(
    private catalogosService: CatalogoService,
    private brigadaService: BrigadaService,
    private toastService: ToastService,
    private asistenciaService: AsistenciaService,
    private diaService: DiaService,
    private reportespdfService: ReportespdfService) { }

  ngOnInit() {
    this.rol = JSON.parse( localStorage.getItem('colaborador') ).role[0];
    this.idUsuarioLogeado = JSON.parse( localStorage.getItem('colaborador') ).id_colaborador;
    this.cargarBrigadasPorRolColaborador();
    this.cargarCatalogoLeyenda();
  }

  cargarCatalogoLeyenda() {
    this.loadingAsistencia = true;
    this.catalogosService.getCatalogo('LEYENDA_ASISTENCIA').subscribe(
      data => {
        this.valoresAsistencia = data;
      },
        err => console.log(err)
      ).add( () => {
        this.loadingAsistencia = false;
      });
    return;
  }

  cargarTablaAsistencias() {

    if ( this.fechaSeleccionada == null) {
      return true;
    }

    let arrayKeys: any[];
    // tslint:disable-next-line:prefer-const
    let arrayColumns: any = [
      { field: 'NOMBRES', header: 'Colaborador'}
    ];
    let contador = 1;

    this.loadingAsistencia = true;
    this.asistenciaService.getTablaAsistencia(
        this.brigadaSeleccionada,
        (this.fechaSeleccionada.getMonth() + 1),
        this.fechaSeleccionada.getFullYear()
      ).subscribe(
      data => {
        arrayKeys = Object.keys(data[0]);
        for (const i of arrayKeys) {
          if ( i.startsWith('dia_')) {
            arrayColumns.push({field: i, header: contador});
            contador++;
          }
        }
        this.valAsistencias = data;
        this.columnasTablaAsistencia = arrayColumns;
      },
        err => console.log(err)
      ).add( () => {
        this.loadingAsistencia = false;
      });
    return;
  }

  limpiarSeleccion() {
    this.asistenciaSeleccionada = null;
  }

  guardarCambioAsistencia(rowData: any, field: any, rowIndex: any) {

    /*
    if ( this.asistenciaSeleccionada.valor1 === '-') {
      this.toastService.verificarRespuesta('INFO', 'Debe seleccionar una opción válida');
      return;
    }
    */

    const pDia: Dia = {
      id_colaborador: rowData.id_colaborador,
      id_brigada: this.brigadaSeleccionada.id_brigada,
      dia : field.replace('dia_', ''),
      mes: this.fechaSeleccionada.getMonth() + 1 + '',
      año: this.fechaSeleccionada.getFullYear() + '',
      dia_trabajado: this.asistenciaSeleccionada.valor1
    };

    this.loadingAsistencia = true;
    this.diaService.registrarDia(pDia).subscribe(
      ( response: RespuestaAPI ) => {
          this.respuesta = response;
          this.valAsistencias[rowIndex][field] = this.respuesta.objeto.dia_trabajado;
          this.valAsistencias[rowIndex]['color_' + field] = this.respuesta.objeto.color;
          this.toastService.verificarRespuesta(this.respuesta.estado, this.respuesta.mensaje);
      },
        err => console.log(err)
      ).add( () => {
        this.loadingAsistencia = false;
      });

    this.limpiarSeleccion();

    return;
  }

  cargarBrigadasPorRolColaborador() {
    // listaBrigadasPorRol
    this.brigadaService.getBrigadasPorRolColaborador(this.rol, this.idUsuarioLogeado).subscribe(
      data => {
        if ( Array.isArray(data) ) {
          this.listaBrigadasUsuario = data;
          this.brigadaSeleccionada = this.listaBrigadasUsuario[0];
        } else {
          this.listaBrigadasUsuario = [];
          this.listaBrigadasUsuario.push(data);
          this.brigadaSeleccionada = data;
        }
        this.cargarTablaAsistencias();
      },
      err => console.log(err)
    );
  }

  exportToEXCEL() {
    this.reportespdfService.getInformeAsistencia(
      this.brigadaSeleccionada,
      (this.fechaSeleccionada.getMonth() + 1),
      this.fechaSeleccionada.getFullYear()
    );
  }

  seleccionarAnterior( seleccionado: string ) {
    this.valoresAsistencia.forEach(
      elemento => {
        if ( elemento.valor1 == seleccionado ) {
          this.asistenciaSeleccionada = elemento;
        }
      }
    );
  }
}
