import { CommonModule } from '@angular/common';
import { HttpClientModule, HttpClient, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LanguageTranslationModule } from './shared/modules/language-translation/language-translation.module';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AuthGuard } from './shared';
import { DataService } from './modelos/data.service';
import { RoleGuard } from './services/role-guard.service';
import { ToastModule } from 'primeng/toast';
import { MessageService, DynamicDialogRef, DynamicDialogConfig, ConfirmationService } from 'primeng/api';
import { DynamicDialogModule } from 'primeng/components/dynamicdialog/dynamicdialog';
import { NuevaActividadComponent } from './layout/components/nueva-actividad/nueva-actividad.component';
import { UpdateDateInterceptor } from './services/update-date.interceptor';

@NgModule({
    imports: [
        CommonModule,
        BrowserModule,
        BrowserAnimationsModule,
        HttpClientModule,
        LanguageTranslationModule,
        AppRoutingModule,
        ToastModule,
        DynamicDialogModule
    ],
    declarations: [AppComponent],
    providers: [AuthGuard, DataService, RoleGuard, MessageService, DynamicDialogRef, DynamicDialogConfig, ConfirmationService],
        //  { provide: HTTP_INTERCEPTORS, useClass: UpdateDateInterceptor, multi: true }],
    bootstrap: [AppComponent]
})
export class AppModule {}
