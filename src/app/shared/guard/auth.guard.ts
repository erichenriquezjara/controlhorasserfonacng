import { Injectable } from '@angular/core';
import { CanActivate, NavigationEnd, RouterStateSnapshot, ActivatedRouteSnapshot } from '@angular/router';
import { Router } from '@angular/router';
import { filter } from 'rxjs/operators';
import { AuthenticationService } from 'src/app/services/authentication.service';

@Injectable()
export class AuthGuard implements CanActivate {

    constructor(private router: Router,
                private authenticationService: AuthenticationService) {}

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const currentUser = this.authenticationService.currentUserValue;
        let validado = false;
        // const menus = currentUser.menus;
        // const submenus = menus[2].submenus;
        // console.log(submenus);
        // console.log(state.url);
        // console.log(currentUser);
        // console.log(route.data);
        if (currentUser) {

            for (const rol of currentUser.role) {
                // tslint:disable-next-line:max-line-length
                console.log(route);
                console.log(rol.nombre_rol);
                console.log(validado);
                // tslint:disable-next-line:triple-equals
                if (route.data.roles && route.data.roles.indexOf(rol.nombre_rol) != -1 && !validado) {
                    console.log('return true');
                    return true;
                } else {
                    validado = false;
                }
            }
        } else {
            validado = false;
        }
        if (validado === false) {
            this.router.navigate(['/login']);
        }
        console.log(validado);
        return validado;
    }
}
