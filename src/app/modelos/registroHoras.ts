export class RegistroHoras {
    id_registro?: number;
    id_siniestro?: number;
    id_brigada?: number;
    horas_extras?: number;
    horas_extras_cr?: number;
    t_horas_extras_cr?: number;
    horas_extras_sr?: number;
    t_horas_extras_sr?: number;
    fecha_horas_extras?: Date;
}