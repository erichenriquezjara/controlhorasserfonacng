import { Colaborador } from './colaborador.model';
import { Brigada } from './brigada.model';
import { Catalogo } from './catalogo.model';

export class Siniestro {
    id_siniestro: number;
    nro_foco?: string;
    codigo_actividad?: number;
    id_brigada?: number;
    centro_costo?: number;
    lugar_destino?: number;
    fuerza_combate?: number;
    fuerza_combate_real?: number;
    fecha_despacho?: Date;
    fecha_salida?: Date;
    fecha_arribo?: Date;
    fecha_inicio?: Date;
    fecha_termino?: Date;
    fecha_regreso?: Date;
    fecha_arribo_base?: Date;
    fecha_creacion?: Date;
    usuario_creacion?: number;
    usuario_modificacion?: number;
    fecha_modificacion?: Date;
    usuario_cancelacion?: number;
    fecha_cancelacion?: Date;
    cancelacion?: string;
    editable?: number;
    horas_extras_sr?: number;
    t_horas_extras_sr?: number;
    horas_extras_cr?: number;
    t_horas_extras_cr?: number;

    // Arreglo de Involucrados asociados a la actividad/siniestro
    colaboradores?: Colaborador[];
    brigada?:  Brigada;
    lugar_destino_cat?: Catalogo;
}
