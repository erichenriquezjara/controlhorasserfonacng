export class Menu {
    id_menu: number;
    nombre?: string;
    link?: string;
    tipo?: string;
    id_padre?: number;
    iclass: string;
    submenus: Menu;
}
