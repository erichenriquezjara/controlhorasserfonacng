export enum Role {
    Supervisor = 'Supervisor',
    Admin = 'Administrador',
    JefeBrigada = 'Jefe Brigada'
}
