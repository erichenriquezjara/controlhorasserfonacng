import { Catalogo } from './catalogo.model';
import { Rol } from './rol.model';
import { Menu } from './menu.model';

export class Colaborador {
    id_colaborador: number;
    rut: string;
    contrasena: string;
    nombres: string;
    apellido_paterno: string;
    apellido_materno: string;
    cargo?: Catalogo;
    es_usuario: number;
    fecha_creacion?: string;
    fecha_modificacion?: string;
    cancelacion?: string;
    role?: Rol[] = null;
    menus?;
}
