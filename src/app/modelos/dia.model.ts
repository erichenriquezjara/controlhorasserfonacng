export class Dia {
    id_dia?: number;
    id_colaborador: number;
    id_brigada: number;
    dia: string;
    mes: string;
    año: string;
    dia_trabajado: string;
}
