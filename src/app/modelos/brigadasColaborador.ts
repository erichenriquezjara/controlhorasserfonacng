import { Brigada } from './brigada.model';
import { Colaborador } from './colaborador.model';

export class BrigadasColaborador {
    brigadas: Brigada[];
    colaborador: Colaborador;
}
