export class Rol {
    id_rol: number;
    nombre_rol: string;
    fecha_creacion?: string;
    usuario_creacion?: string;
    usuario_modificacion?: string;
    fecha_modificacion?: string;
    fecha_cancelacion?: string;
    cancelacion?: string;
}
