import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { LoginRoutingModule } from './login-routing.module';
import { LoginComponent } from './login.component';
import { FormsModule } from '@angular/forms';
import { NgBusyModule } from 'ng-busy';
import { NgxLoadingModule, ngxLoadingAnimationTypes } from 'ngx-loading';

@NgModule({
    imports: [
        CommonModule,
        TranslateModule,
        LoginRoutingModule,
        FormsModule,
        NgBusyModule.forRoot({
            message: 'Por Favor, espere.',
            delay: 50,
            minDuration: 600,
            templateNgStyle: { 'background-color': 'black' }
          }),
        NgxLoadingModule.forRoot({
        animationType: ngxLoadingAnimationTypes.circleSwish
        })
    ],
    declarations: [LoginComponent]
})
export class LoginModule {}
