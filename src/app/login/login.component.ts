import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { routerTransition } from '../router.animations';
import { LoginService } from '../services/login.service';
import { Colaborador } from '../modelos/colaborador.model';
import { RolService } from '../services/rol.service';
import { Rol } from '../modelos/rol.model';
import { ColaboradorService } from '../services/colaborador.service';
import * as CryptoJS from 'crypto-js';
import { NgForm } from '@angular/forms';
import { Subscription } from 'rxjs';
import { AuthenticationService } from '../services/authentication.service';
import { first } from 'rxjs/operators';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss'],
    animations: [routerTransition()]
})
export class LoginComponent implements OnInit {
    colaboradores: Colaborador[];
    islogged = false;
    formEnviado = false;
    roles: Rol[];
    usuario: string;

    loading = true;
    error = '';

    returnUrl: any;

    loadingLogin = false;

    constructor(
      public router: Router,
      private route: ActivatedRoute,
      private loginService: LoginService,
      private rolService: RolService,
      private colaboradorService: ColaboradorService,
      private authenticationService: AuthenticationService
    ) {
        // if (this.authenticationService.currentUserValue) {
        //     this.router.navigate(['/dashboard']);
        // }
    }

    ngOnInit() {
    }

    // Para hacer login apretando la tecla Enter en el Password.
    evaluarTecla(event: KeyboardEvent, signupForm: NgForm) {
        if (event.keyCode === 13) {
            this.onSubmit(signupForm);
        }
    }

    onSubmit(form: NgForm) {
        form.value.contrasena = CryptoJS.MD5(form.value.contrasena).toString();
        console.log('inicio onSubmit  - login. ' + new Date());

        this.loadingLogin = true;
        this.authenticationService.login(form.value)
            .pipe(first())
            .subscribe(
                data => {
                    this.loadingLogin = false;
                    console.log(data);
                    this.router.navigate(['/dashboard']);
                },
                error => {
                    this.loadingLogin = false;
                    this.error = error;
                    this.loadingLogin = false;
                }

            ).add(() => {
                this.loadingLogin = false;
            });
        console.log('fin onSubmit  - login. ' + new Date());
    }

    // onLoggedin(form: NgForm) {
    //     this.formEnviado = true;

    //     if (form.valid) {
    //         const usuario = form.value.usuario;
    //         form.value.contrasena = CryptoJS.MD5(form.value.contrasena).toString();
    //         // console.log('COntraseña encrptada :' + password);
    //         // console.log('COntraseña texto plano :' + form.value.contrasena);
    //         console.log('form.value: ' + form);
    //         this.busy = this.loginService.getLogin(form.value).subscribe(data => {
    //             this.islogged = data;
    //             console.log('isLogged: ' + this.islogged['resultado']);
    //             if (Boolean(this.islogged['resultado'])) {
    //                 this.colaboradorService.getColaboradoresporRut(usuario).subscribe(dat => {
    //                     this.colaboradores = dat;
    //                     localStorage.setItem('colaborador', JSON.stringify(this.colaboradores));
    //                     this.obtenerRoles(this.colaboradores[0].id_colaborador);
    //                 }
    //             );
    //             }
    //         });
    //         return;
    //     }

    // }

    // obtenerRoles(idColaborador: number) {
    //     return this.rolService.getRolesbyColaborador(idColaborador).subscribe(data => {
    //         this.roles = data;
    //         console.log('Data: ' + data);
    //         localStorage.setItem('roles', JSON.stringify(this.roles));
    //         this.router.navigate(['/dashboard']);
    //     });
    //   }
}
