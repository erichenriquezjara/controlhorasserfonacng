import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Rol } from '../modelos/rol.model';
import { RespuestaAPI } from '../modelos/respuestaAPI.model';
import { ColaboradorRol } from '../modelos/colaboradorRol.model';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class RolService {

  apiUrl = environment._urlApi + '/rol';

  constructor(private _http: HttpClient) { }

  getRoles(id: number = 0) {
    return this._http.get<Rol[]>(this.apiUrl + '/' + (id !== 0 ? id.toString() : ''));
  }

  registrarRol(col: Rol) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json'
      })
    };

    let response;

    if ( col.id_rol === undefined) {
      response = this._http.post<RespuestaAPI>(this.apiUrl, col, httpOptions);
    } else {
      response = this._http.put<RespuestaAPI>(this.apiUrl, col, httpOptions);
    }

    return response;
  }

  getRolesbyColaborador(id: number) {
    return this._http.get<Rol[]>(this.apiUrl + '/rolesColaborador/' + (id !== 0 ? id.toString() : ''));
  }

  getRolesbyRut(rut: string) {
    return this._http.get<Rol[]>(this.apiUrl + '/rolesRut/' + (rut !== null ? rut.toString() : ''));
  }

  getRolessinAsignar(rut: string) {
    return this._http.get<Rol[]>(this.apiUrl + '/rolesSinAsignar/' + (rut !== null ? rut.toString() : ''));
  }

  guardarRolesUsuario(colaboradorRol: ColaboradorRol[]) {
    let response;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json'
      })
    };
    // console.log(colaboradorRol);
    response = this._http.post<ColaboradorRol>(this.apiUrl + '/guardarRolesUsuario', colaboradorRol, httpOptions);
    console.log(response);
    return response;
  }

  eliminarRol(col: Rol) {
    col.cancelacion = 'S';
    return this.registrarRol(col);
  }
}
