import { TestBed } from '@angular/core/testing';

import { SiniestrosService } from './siniestros.service';

describe('SiniestrosService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SiniestrosService = TestBed.get(SiniestrosService);
    expect(service).toBeTruthy();
  });
});
