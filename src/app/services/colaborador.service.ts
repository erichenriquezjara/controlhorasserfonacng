import { Injectable } from '@angular/core';
import { Colaborador } from '../modelos/colaborador.model';
import { Brigada } from '../modelos/brigada.model';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { RespuestaAPI } from '../modelos/respuestaAPI.model';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})

export class ColaboradorService {

  apiUrl = environment._urlApi + '/colaborador';


  constructor(private _http: HttpClient) { }

  getColaboradores(id: number = 0) {
    return this._http.get<Colaborador[]>(this.apiUrl + '/' + (id !== 0 ? id.toString() : ''));
  }

  getColaboradoresPorRol(rol: string) {
    return this._http.get<Colaborador[]>(this.apiUrl + '/porRol/' + rol);
  }

  getColaboradoresSinBrigada() {
    return this._http.get<Colaborador[]>(this.apiUrl + '/sinbrigada');
  }

  getColaboradoresPorBrigada(bri: Brigada) {
    return this._http.get<Colaborador[]>(this.apiUrl + '/porbrigada/' + bri.id_brigada);
  }

  getColaboradoresporRut(rut: string) {
    return this._http.get<Colaborador[]>(this.apiUrl + '/colaboradorporrut/' + rut);
  }

  getJefesBrigada() {
    return this._http.get<Colaborador[]>(this.apiUrl + '/jefeBrigadas/' );
  }

  registrarColaborador(col: Colaborador) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json'
      })
    };

    let response;

    if ( col.id_colaborador === undefined) {
      response = this._http.post<RespuestaAPI>(this.apiUrl, col, httpOptions);
    } else {
      response = this._http.put<RespuestaAPI>(this.apiUrl, col, httpOptions);
    }

    return response;
  }

  eliminarColaborador(col: Colaborador) {
    console.log('Eliminando ' + col.id_colaborador);
    col.cancelacion = 'S';
    return this.registrarColaborador(col);
  }

  colaboradoresSiniestro(id: number = 0) {
    return this._http.get<Colaborador[]>(this.apiUrl + '/colaboradorSiniestro/' + (id !== 0 ? id.toString() : ''));
  }

  colaboradorBrigada(id: number = 0) {
    return this._http.get<Colaborador>(this.apiUrl + '/colaboradorBrigada/' + (id !== 0 ? id.toString() : ''));
  }
}
