import { TestBed } from '@angular/core/testing';

import { ReportespdfService } from './reportespdf.service';

describe('ReportespdfService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ReportespdfService = TestBed.get(ReportespdfService);
    expect(service).toBeTruthy();
  });
});
