import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Menu } from '../modelos/menu.model';

@Injectable({
  providedIn: 'root'
})
export class MenuService {

  apiUrl = environment._urlApi + '/menu';
  logueado = false;

  // login = [{rut: '16329970-4', contrasena: '123456'}];

  constructor(private _http: HttpClient) { }

  getSiniestros(id: number = 0) {
    return this._http.get<Menu[]>(this.apiUrl + '/menusporrol/1');
  }
}
