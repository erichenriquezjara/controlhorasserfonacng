import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Dia } from '../modelos/dia.model';
import { RespuestaAPI } from '../modelos/respuestaAPI.model';

@Injectable({
  providedIn: 'root'
})
export class DiaService {

  apiUrl = environment._urlApi + '/dia';

  constructor(private _http: HttpClient) { }

  registrarDia(pDia: Dia) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json'
      })
    };

    let response;
    response = this._http.post<RespuestaAPI>(this.apiUrl, pDia, httpOptions);
    return response;
  }
}
