import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Catalogo } from '../modelos/catalogo.model';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CatalogoService {

  apiUrl = environment._urlApi + '/catalogo';


  constructor(private _http: HttpClient) { }

  getCatalogo(nombreCatalogo: string = '') {
    return this._http.get<Catalogo[]>(this.apiUrl + '/' + encodeURI(nombreCatalogo));
  }
}
