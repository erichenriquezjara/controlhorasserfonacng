import { Injectable } from '@angular/core';
import { Colaborador } from '../modelos/colaborador.model';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  apiUrl = environment._urlApi + '/login';
  logueado = false;

  // login = [{rut: '16329970-4', contrasena: '123456'}];

  constructor(private _http: HttpClient) { }

  getLogin(login: any) {
    console.log('Login' + login);
    return this._http.post<boolean>(this.apiUrl, login);
  }

  public logOut() {
    localStorage.removeItem('colaborador');
    localStorage.removeItem('roles');
  }
}
