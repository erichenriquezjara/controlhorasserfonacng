import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { RegistroHoras } from '../modelos/registroHoras';
import { RespuestaAPI } from '../modelos/respuestaAPI.model';

@Injectable({
  providedIn: 'root'
})
export class HorasExtrasService {

  apiUrl = environment._urlApi + '/registro_horas';
  constructor(private _http: HttpClient) { }

  guardarHorasExtras(horasExtras: RegistroHoras){
      const httpOptions = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json'
        })
      };
  
      let response: any;
      if (horasExtras.id_registro === undefined) {
        response = this._http.post<RespuestaAPI>(this.apiUrl, horasExtras, httpOptions);
      } else {
        response = this._http.put<RespuestaAPI>(this.apiUrl, horasExtras, httpOptions);
      }
  
      return response;
  }

  obtenerHorasExtrasBrigada(idBrigda, fDesde, fHasta){
    return this._http.get<RegistroHoras[]>(this.apiUrl + '/porBrigada/' + idBrigda.toString() + '/' + fDesde + '/' + fHasta);
  }

  horasExtrasPorZona(idColaborador, fDesde, fHasta){
    return this._http.get<RegistroHoras[]>(this.apiUrl + '/horasExtrasPorZona/' + idColaborador + '/' + fDesde + '/' + fHasta);
  }  

  probando(){
    return 'probando...';
  }

}
