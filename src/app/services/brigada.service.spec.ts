import { TestBed } from '@angular/core/testing';

import { BrigadaService } from './brigada.service';

describe('BrigadaService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: BrigadaService = TestBed.get(BrigadaService);
    expect(service).toBeTruthy();
  });
});
