import { Injectable } from '@angular/core';

import { Brigada } from '../modelos/brigada.model';
import { Colaborador } from '../modelos/colaborador.model';
import { BrigadaColaboradores } from '../modelos/brigadaColaboradores';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { RespuestaAPI } from '../modelos/respuestaAPI.model';
import { environment } from 'src/environments/environment';
import { BrigadasColaborador } from '../modelos/brigadasColaborador';
import { Rol } from '../modelos/rol.model';

@Injectable({
  providedIn: 'root'
})
export class BrigadaService {

  apiUrl = environment._urlApi + '/brigada';

  constructor(private _http: HttpClient) { }

  getBrigadas(id: number = 0) {
    return this._http.get<Brigada[]>(this.apiUrl + '/' + (id !== 0 ? id.toString() : ''));
  }

  getBrigadaPorJefe(id: number = 0) {
    return this._http.get<Brigada>(this.apiUrl + '/brigadaPorJefe/' + id);
  }

  getBrigadasPorSupervisor(id_colaborador: number) {
    return this._http.get<Brigada[]>(this.apiUrl + '/brigadaPorSupervisor/' + id_colaborador);
  }

  getBrigadasPorRolColaborador(rol: Rol, idCol: number) {
    return this._http.get<Brigada[]>(this.apiUrl + '/brigadasPorRolColaborador/' + rol.id_rol + '/' + idCol);
  }

  getBrigadasSinSupervisor() {
    return this._http.get<Brigada[]>(this.apiUrl + '/brigadaSinAsignar/');
  }

  registrarBrigada(bri: Brigada) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };

    let response: any;
    if (bri.id_brigada === undefined) {
      response = this._http.post<RespuestaAPI>(this.apiUrl, bri, httpOptions);
    } else {
      response = this._http.put<RespuestaAPI>(this.apiUrl, bri, httpOptions);
    }

    return response;
  }

  eliminarBrigada(bri: Brigada) {
    bri.cancelacion = 'S';
    return this.registrarBrigada(bri);
  }

  registrarIntegrantesBrigada(bri: Brigada, colaboradores: Colaborador[]) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };

    let response: any;
    const briCol: BrigadaColaboradores = new BrigadaColaboradores();
    if (bri.id_brigada !== undefined) {
      briCol.brigada = bri;
      briCol.colaboradores = colaboradores;
      response = this._http.put<RespuestaAPI>(this.apiUrl + '/integrantes', briCol, httpOptions);
    }

    return response;
  }

  registrarBrigadasPorSupervisor(brigadas: Brigada[], colaborador: Colaborador) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };

    let response: any;
    const briCol: BrigadasColaborador = new BrigadasColaborador();
    briCol.brigadas = brigadas;
    briCol.colaborador = colaborador;
    response = this._http.put<RespuestaAPI>(this.apiUrl + '/registrarBrigadasPorSupervisor', briCol, httpOptions);
    return response;
  }
}
