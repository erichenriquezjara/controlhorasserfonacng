import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Utils } from 'src/environments/utils';
import * as moment from 'moment';
import { Brigada } from '../modelos/brigada.model';

@Injectable({
  providedIn: 'root'
})
export class ReportespdfService {

  urlReportes = environment._urlReportes + '/Informes';

  constructor(private _http: HttpClient) { }

  getInformeHorasExtra(idBrigada: number = -1, fDesde: Date, fHasta: Date) {
    if (fDesde != null && fHasta != null) {
      window.open(this.urlReportes +
        '/horasExtra1/-1/' +
        idBrigada + '/' +
        btoa(Utils.toDate(fDesde)) + '/' +
        btoa(Utils.toDate(fHasta))
        , 'Informe');
    } else {
      window.open(this.urlReportes +
        '/horasExtra1/-1/' +
        idBrigada
        , 'Informe');
    }
  }

  getInformeHorasExtraExcel(idBrigada: number = -1, fDesde: Date, fHasta: Date) {
    if (fDesde != null && fHasta != null) {
      window.open(this.urlReportes +
        '/horasExtrasPorBrigadaExcel/-1/' +
        idBrigada + '/' +
        btoa(Utils.toDate(fDesde)) + '/' +
        btoa(Utils.toDate(fHasta))
        , 'Informe');
    } else {
      window.open(this.urlReportes +
        '/horasExtrasPorBrigadaExcel/-1/' +
        idBrigada
        , 'Informe');
    }
  }  

  getInformeAsistencia(brigada: Brigada, mes: any, año: any) {
    window.open(this.urlReportes + '/obtenerAsistenciaBrigadaMes/' + brigada.id_brigada + '/' + mes +  '/' + año );
  }

  getInformeHorasExtraBrigada(idBrigada: number = -1, fDesde: Date, fHasta: Date) {

    let sfDesde = moment(fDesde).format('YYYY-MM-DD');
    let sfHasta = moment(fHasta).format('YYYY-MM-DD');

    if (fDesde != null && fHasta != null) {
      window.open(this.urlReportes +
        '/horasExtraPorBrigada/' +
        idBrigada + '/' +
        btoa(sfDesde) + '/' +
        btoa(sfHasta)
        , 'Informe');
    }
  }  

  getInformeActividades1(idBrigada: number = -1, fDesde: Date, fHasta: Date) {
    if (fDesde != null && fHasta != null) {
      window.open(this.urlReportes +
        '/actividades1/-1/' +
        idBrigada + '/' +
        btoa(Utils.toDate(fDesde)) + '/' +
        btoa(Utils.toDate(fHasta))
        , 'Informe');
    } else {
      window.open(this.urlReportes +
        '/actividades1/-1/' +
        idBrigada
        , 'Informe');
    }
  }
}
