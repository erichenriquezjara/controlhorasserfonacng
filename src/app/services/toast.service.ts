import { Injectable } from '@angular/core';
import { MessageService } from 'primeng/api';

@Injectable({
  providedIn: 'root'
})
export class ToastService extends MessageService {

  constructor ( private messageService: MessageService ) {
    super();
  }

  correcto(mensaje) {
    this.messageService.add({severity: 'success', summary: 'Éxito', detail: mensaje});
  }

  error(mensaje) {
    this.messageService.add({severity: 'error', summary: 'Error', detail: mensaje});
  }

  advertencia(mensaje) {
    this.messageService.add({severity: 'warn', summary: 'Advertencia', detail: mensaje});
  }

  informacion(mensaje) {
    this.messageService.add({severity: 'info', summary: 'Información', detail: mensaje});
  }

  // Acá podemos enviar la respuesta directa de la API, de esta forma evitamos crear un mensaje para cada componente.
  verificarRespuesta(estado: string, mensaje: string) {
    switch ( estado ) {
      case 'OK':
        this.correcto(mensaje);
      break;
      case 'ERROR':
        this.error(mensaje);
      break;
      case 'INFO':
        this.informacion(mensaje);
      break;
      default:
        // En este caso el DEFAULT nos ayuda a saber si la API trajo una respuesta con la estructura correcta.
        this.error('El servidor respondió un mensaje con la estructura Incorrecta: Verificar.');
      break;
    }
  }
}
