import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { Colaborador } from '../modelos/colaborador.model';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  private currentUserSubject: BehaviorSubject<Colaborador>;
  public currentUser: Observable<Colaborador>;

  apiUrl = environment._urlApi + '/login';

  constructor(private http: HttpClient) {
    this.currentUserSubject = new BehaviorSubject<Colaborador>(JSON.parse(localStorage.getItem('colaborador')));
    this.currentUser = this.currentUserSubject.asObservable();
   }

   public get currentUserValue(): Colaborador {
     return this.currentUserSubject.value;
   }

   login(login) {
      return this.http.post<any>(this.apiUrl, login)
        .pipe(map( user => {
          if (user)  {
            localStorage.setItem('colaborador', JSON.stringify(user));
            this.currentUserSubject.next(user);
          }
          return user;
        }));
   }

   logout() {
     localStorage.removeItem('colaborador');
     this.currentUserSubject.next(null);
   }
}
