import { TestBed } from '@angular/core/testing';

import { HorasExtrasService } from './horas-extras.service';

describe('HorasExtrasService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: HorasExtrasService = TestBed.get(HorasExtrasService);
    expect(service).toBeTruthy();
  });
});
