import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Siniestro } from '../modelos/siniestro.model';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { RespuestaAPI } from '../modelos/respuestaAPI.model';
import { Utils } from 'src/environments/utils';
import * as moment from 'moment';
import 'moment-timezone';

@Injectable({
  providedIn: 'root'
})
export class SiniestrosService {


  apiUrl = environment._urlApi + '/siniestro';

  constructor(private _http: HttpClient) { }

  getSiniestros(id: number = 0) {
    return this._http.get<Siniestro[]>(this.apiUrl + '/' + (id !== 0 ? id.toString() : ''));
  }

  getSiniestrosPorBrigadaFechas(idBrigada: number = -1, fDesde: Date, fHasta: Date) {
    if (fDesde != null && fHasta != null) {
      return this._http.get<Siniestro[]>(
        this.apiUrl + '/siniestrosPorBrigada/-1/' + idBrigada + '/' + btoa(Utils.toDate(fDesde)) + '/' + btoa(Utils.toDate(fHasta))
      );
    } else {
      return this._http.get<Siniestro[]>(this.apiUrl + '/siniestrosPorBrigada/-1/' + idBrigada);
    }
  }

  registrarSiniestro(siniestro: Siniestro) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };

    let response;

    if (siniestro.id_siniestro === undefined) {
      response = this._http.post<RespuestaAPI>(this.apiUrl, siniestro, httpOptions);
    } else {
      response = this._http.put<RespuestaAPI>(this.apiUrl, siniestro, httpOptions);
    }

    return response;
  }

  actualizarSiniestro(siniestro: Siniestro) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };

    let response;
    console.log('SINIESTRO');
    console.log(siniestro);
    response = this._http.put<RespuestaAPI>(this.apiUrl + '/actualizarSiniestro', siniestro, httpOptions);
    return response;
  }

  eliminarSiniestro(siniestro: Siniestro) {
    siniestro.cancelacion = 'S';
    return this.actualizarSiniestro(siniestro);
  }

}
