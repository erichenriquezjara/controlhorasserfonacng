import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Brigada } from '../modelos/brigada.model';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AsistenciaService {
  apiUrl = environment._urlApi + '/dia';
  constructor(private _http: HttpClient) { }
  getTablaAsistencia(brigada: Brigada, mes: any, año: any) {
    return this._http.get<any[]>(this.apiUrl + '/obtenerAsistenciaBrigadaMes/' + brigada.id_brigada + '/' + mes +  '/' + año );
  }
}
