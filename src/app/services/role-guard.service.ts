
import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router, Route } from '@angular/router';
import { Observable } from 'rxjs';
import { RolService } from './rol.service';
import { Rol } from '../modelos/rol.model';
import { Colaborador } from '../modelos/colaborador.model';
import { RolesComponent } from '../layout/mantenedores/roles/roles.component';
import { LoginService } from './login.service';

@Injectable()
export class RoleGuard implements CanActivate {

  isValid = false;
  roles: Rol[] = JSON.parse(localStorage.getItem('roles'));
  constructor(private rolService: RolService, private _router: Router, private loginService: LoginService) {
  }

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    const valid = this;
    valid.isValid = false;
    Object.values(next.data).forEach(function(pathRol) {
      console.log(state.url);
      if (pathRol.path === state.url) {
        pathRol.role.forEach(function(rolForPath) {
          console.log(rolForPath);
          valid.roles.forEach(function(rolforUser) {
            console.log(rolforUser);
            if (rolforUser.nombre_rol === rolForPath && !valid.isValid) {
              valid.isValid = true;
            }
          });

      });
    }
  });
    if (!this.isValid) {
       this.redirigir();
    }
     return this.isValid;
  }

  redirigir() {
    this.loginService.logOut();
    this._router.navigate(['/login']);
  }

}
