import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';

@Pipe({
  name: 'horasextras'
})
export class HorasextrasPipe implements PipeTransform {

  transform(value: any, horaInicio: string, horaFin: string): any {
    const jornadas = [
      {
        inicioJornadaHora: 11,
        inicioJornadaMinuto: 0,
        finJornadaHora: 12,
        finJornadaMinuto: 0
      },
      {
        inicioJornadaHora: 13,
        inicioJornadaMinuto: 0,
        finJornadaHora: 19,
        finJornadaMinuto: 30
      }
    ];
    let horasExtras = 0;
    const inicioActividad = moment(horaInicio, 'YYYY-MM-DD HH:mm');
    const finActividad = moment(horaFin, 'YYYY-MM-DD HH:mm');

    if (inicioActividad.isBefore(finActividad)) {
      // console.log('hora de inicio menor a la hora de fin');
      let dia_actual = inicioActividad.clone();
      let diaActualComparar = inicioActividad.clone();

      while (dia_actual.isSameOrBefore(finActividad)) {
        // console.log('******************** Inicio dia ********************');
        let horaInicioActual = dia_actual.clone();
        let horaFinActual = null;
        diaActualComparar = dia_actual;
        // console.log('horaInicioActual: ' + horaInicioActual.format('YYYY-MM-DD HH:mm'));
        // Se comprueba si el fin de la actividad está dentro del día, si no lo está se valoriza la hora de fin de la 
        // actividad con la última hora del día
        if (finActividad.isSameOrBefore(diaActualComparar.set({ 'hour': 23, 'minute': 59, 'second': 59 }))) {
          horaFinActual = finActividad.clone();
        } else {
          horaFinActual = diaActualComparar.set({ 'hour': 23, 'minute': 59, 'second': 59 }).clone();
        }
        // console.log('Día actual: ' + dia_actual.format('YYYY-MM-DD HH:mm'));
        // console.log(horaInicioActual.format('YYYY-MM-DD HH:mm') + ' / ' + horaFinActual.format('YYYY-MM-DD HH:mm'));
        let horaInicioTemp = horaInicioActual.clone();
        let horaFinTemp = horaFinActual.clone();
        // Variable para marcar si una actividad terminó antes de recorrer todas las jornadas laborales.
        let finActividadFlag = false;

        jornadas.forEach(function (element, i) {
          // console.log('======= JORNADA ' + i + ' =============');
          let inicioJornada = moment(horaInicioActual).set({
            'hour': element.inicioJornadaHora, 'minute': element.inicioJornadaMinuto, 'second': 0
          }).clone();
          let finJornada = moment(horaFinActual).set({
            'hour': element.finJornadaHora, 'minute': element.finJornadaMinuto, 'second': 0
          }).clone();


          if (horaInicioTemp.isSameOrAfter(inicioJornada)) {
            // console.log('no se suman horas extras [inicioActividad >= inicioJornada]');
          } else {
            // Comprobar si la hora de fin de actividad es menor al inicio de la jornada, entonces
            // calcular la diferencia entre inicio de la actividad y el fin de la actividad.
            // Si se cumple la condición significa que la actividad ya terminó para este día y no debe continuar con
            // la siguiente jornada.
            if (horaFinTemp.isSameOrBefore(inicioJornada) && horaInicioTemp.isSameOrBefore(inicioJornada) && !finActividadFlag) {
              // console.log('Hora fin de la actividad menor que la hora de inicio de jornada: Horas extras: ' + horaFinTemp.diff(horaInicioTemp, 'seconds'));
              horasExtras += horaFinTemp.diff(horaInicioTemp, 'seconds');
              finActividadFlag = true;
            } else if (!finActividadFlag) {
              // console.log('Se suman horas extras [else - inicioActividad >= inicioJornada]');
              // console.log(inicioJornada.diff(horaInicioTemp, 'seconds'));
              horasExtras += inicioJornada.diff(horaInicioTemp, 'seconds');
            }
          }

          if (horaFinTemp.isSameOrBefore(finJornada)) {
            // console.log('no se suman horas extras [finActividad <= finJornada]');
          } else {
            if (jornadas.length == i + 1) {
              // console.log('Se suman horas extras [finActividad <= finJornada]');
              if (horaInicioTemp.isSameOrBefore(finJornada)) {
                // console.log(horaFinTemp.diff(finJornada, 'seconds'));
                horasExtras += horaFinTemp.diff(finJornada, 'seconds');
              } else {
                // console.log(horaFinTemp.diff(horaInicioTemp, 'seconds'));
                horasExtras += horaFinTemp.diff(horaInicioTemp, 'seconds');
              }
            } else if (horaInicioTemp.isSameOrBefore(finJornada)) {
              horaInicioTemp = finJornada.clone();
            }
          }
          if (horaInicioTemp.isSameOrBefore(finJornada)) {
            horaInicioTemp = finJornada.clone();
          }
          // console.log('======= FIN JORNADA =============');
        });

        dia_actual.add({ days: 1 }).set({ 'hour': 0, 'minute': 0, 'second': 0 });
        // console.log('Dia Actual: ' + dia_actual.format('DD-MM-YYYY HH:mm'));
        // console.log('Horas extras dia: ' + horasExtras);
        // console.log('******************** Fin dia ********************');
      }
    }
    // return Math.round(horasExtras / 60);
    return (horasExtras / 3600).toFixed(2);
  }

}
