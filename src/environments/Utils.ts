import * as moment from 'moment';
import 'moment-timezone';
import { RegistroHoras } from 'src/app/modelos/registroHoras';

export class Utils {
    public static toDate(dato: any) {
        if (dato) {
          const fecha: string = moment(dato).format('DD/MM/YYYY HH:mm');
          return fecha;
        } else {
          return null;
        }
      }
    
      public static obtenerHorasExtras(horaInicio: Date, horaFin: Date){

        const jornadas = [
          {
            inicioJornadaHora: 11,
            inicioJornadaMinuto: 0,
            finJornadaHora: 12,
            finJornadaMinuto: 0
          },
          {
            inicioJornadaHora: 13,
            inicioJornadaMinuto: 0,
            finJornadaHora: 19,
            finJornadaMinuto: 30
          }
        ];
        let horaExtra: RegistroHoras = {
          horas_extras_cr : 0,
          t_horas_extras_cr : 0,
          horas_extras_sr : 0,
          t_horas_extras_sr : 0,
          horas_extras: 0
        };
        let arrayHoras: RegistroHoras[];

        let horasExtras = 0.0;
        let horas = 0.0;
        const inicioActividad = moment(horaInicio, 'YYYY-MM-DD HH:mm');
        const finActividad = moment(horaFin, 'YYYY-MM-DD HH:mm');
    
        if (inicioActividad.isBefore(finActividad)) {
          // console.log('hora de inicio menor a la hora de fin');
          let dia_actual = inicioActividad.clone();
          let diaActualComparar = inicioActividad.clone();
    
          while (dia_actual.isSameOrBefore(finActividad)) {
            // console.log('******************** Inicio dia ********************');
            let horaInicioActual = dia_actual.clone();
            let horaFinActual = null;
            diaActualComparar = dia_actual;
            // console.log('horaInicioActual: ' + horaInicioActual.format('YYYY-MM-DD HH:mm'));
            // Se comprueba si el fin de la actividad está dentro del día, si no lo está se valoriza la hora de fin de la 
            // actividad con la última hora del día
            if (finActividad.isSameOrBefore(diaActualComparar.set({ 'hour': 23, 'minute': 59, 'second': 59 }))) {
              horaFinActual = finActividad.clone();
            } else {
              horaFinActual = diaActualComparar.set({ 'hour': 23, 'minute': 59, 'second': 59 }).clone();
            }
            // console.log('Día actual: ' + dia_actual.format('YYYY-MM-DD HH:mm'));
            // console.log(horaInicioActual.format('YYYY-MM-DD HH:mm') + ' / ' + horaFinActual.format('YYYY-MM-DD HH:mm'));
            let horaInicioTemp = horaInicioActual.clone();
            let horaFinTemp = horaFinActual.clone();
            // Variable para marcar si una actividad terminó antes de recorrer todas las jornadas laborales.
            let finActividadFlag = false;

            // Variables para la solución trucha
            let inicioJornada1 = moment(inicioActividad).set({
              'hour': jornadas[0].inicioJornadaHora, 'minute': jornadas[0].inicioJornadaMinuto, 'second': 0
            }).clone();
            let finJornada1 = moment(finActividad).set({
              'hour': jornadas[0].finJornadaHora, 'minute': jornadas[0].finJornadaMinuto, 'second': 0
            }).clone();
            let marcadorTrucho = false;
    
            jornadas.forEach(function (element, i) {
              if(inicioActividad.isSameOrBefore(inicioJornada1) && finActividad.isSameOrAfter(inicioJornada1) && finActividad.isSameOrBefore(finJornada1) && !marcadorTrucho){
                horasExtras += inicioJornada1.diff(inicioActividad, 'seconds');
                marcadorTrucho = true;
              } else if (!marcadorTrucho){
                // console.log('======= JORNADA ' + i + ' =============');
                
                let inicioJornada = moment(horaInicioActual).set({
                  'hour': element.inicioJornadaHora, 'minute': element.inicioJornadaMinuto, 'second': 0
                }).clone();
                let finJornada = moment(horaFinActual).set({
                  'hour': element.finJornadaHora, 'minute': element.finJornadaMinuto, 'second': 0
                }).clone();
      
                
                if (horaInicioTemp.isSameOrAfter(inicioJornada)) {
                  // console.log('no se suman horas extras [inicioActividad >= inicioJornada]');
                } else {
                  // Comprobar si la hora de fin de actividad es menor al inicio de la jornada, entonces
                  // calcular la diferencia entre inicio de la actividad y el fin de la actividad.
                  // Si se cumple la condición significa que la actividad ya terminó para este día y no debe continuar con
                  // la siguiente jornada.
                  if (horaFinTemp.isSameOrBefore(inicioJornada) && horaInicioTemp.isSameOrBefore(inicioJornada) && !finActividadFlag) {
                    // console.log('Hora fin de la actividad menor que la hora de inicio de jornada: Horas extras: ' + horaFinTemp.diff(horaInicioTemp, 'seconds'));
                    horasExtras += horaFinTemp.diff(horaInicioTemp, 'seconds');
                    finActividadFlag = true;
                  } else if (!finActividadFlag) {
                    // console.log('Se suman horas extras [else - inicioActividad >= inicioJornada]');
                    // console.log(inicioJornada.diff(horaInicioTemp, 'seconds'));
                    horasExtras += inicioJornada.diff(horaInicioTemp, 'seconds');
                  }
                }
      
                if (horaFinTemp.isSameOrBefore(finJornada)) {
                  // console.log('no se suman horas extras [finActividad <= finJornada]');
                } else {
                  if (jornadas.length == i + 1) {
                    // console.log('Se suman horas extras [finActividad <= finJornada]');
                    if (horaInicioTemp.isSameOrBefore(finJornada)) {
                      // console.log(horaFinTemp.diff(finJornada, 'seconds'));
                      horasExtras += horaFinTemp.diff(finJornada, 'seconds');
                    } else {
                      // console.log(horaFinTemp.diff(horaInicioTemp, 'seconds'));
                      horasExtras += horaFinTemp.diff(horaInicioTemp, 'seconds');
                    }
                  } else if (horaInicioTemp.isSameOrBefore(finJornada)) {
                    horaInicioTemp = finJornada.clone();
                  }
                }
                if (horaInicioTemp.isSameOrBefore(finJornada)) {
                  horaInicioTemp = finJornada.clone();
                }
                // console.log('======= FIN JORNADA =============');
              }
            });
            
            // console.log("Llamando a la función que graba las horas extras por día y brigada");
            
            horas = parseFloat((horasExtras / 3600).toFixed(2));
            // horaExtra.id_brigada = 3;
            // horaExtra.id_siniestro = 107;
            horaExtra.horas_extras = horas;
            horaExtra.fecha_horas_extras = dia_actual.toDate();
            // this.registrarHoraExtra(this.horaExtra);
            console.log('======= Dentro de la función obtenerhorasextras =======');
            console.log(horaExtra);
            console.log(horas);
            console.log('======= Dentro de la función obtenerhorasextras =======');
            dia_actual.add({ days: 1 }).set({ 'hour': 0, 'minute': 0, 'second': 0 });
            // console.log('Dia Actual: ' + dia_actual.format('DD-MM-YYYY HH:mm'));
            // console.log('Horas extras dia: ' + horasExtras);
            // console.log('******************** Fin dia ********************');
          }
        }
        // return Math.round(horasExtras / 60);
        // return (horasExtras / 3600).toFixed(2);
        return horaExtra;
      }
}
